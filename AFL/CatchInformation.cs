﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace AFL
{
    public class CatchInformation
    {
        public List<Catch> listCatches;

        private const String DATA_FILENAME = "catchInformation.dat";

        public CatchInformation()
        {
            this.listCatches = new List<Catch>();
        }

        public void AddCatch(DateTime Time, Image Image, String Species, String Location, String Bait, float WeatherTemperature, float WindSpeed, String WeatherConditions, float Length, float Weight, String WaterClarity, float Depth, float WaterTemperature, bool Method, String Note)
        {
            this.listCatches.Add(new Catch(Time, Image, Species, Location, Bait, WeatherTemperature, WindSpeed, WeatherConditions, Length, Weight, WaterClarity, Depth, WaterTemperature, Method, Note));
            Console.WriteLine("Catch succesfully added."); //CHANGE LATER
        }

        public void RemoveCatch(DateTime time, String species) //Change whole method later
        {
            foreach (Catch c in listCatches)
            {
                if (c.Time == time && c.Species == species)
                {
                    if (this.listCatches.Remove(c))
                    {
                        Console.WriteLine(time + " has been removed successfully."); //CHANGE LATER
                        return;
                    }
                    else
                    {
                        Console.WriteLine("Unable to remove " + time); //CHANGE LATER
                        return;
                    }
                }
            }
            Console.WriteLine(time + "had not been added before."); //CHANGE LATER
        }

        public void SaveCatches()
        {
            try
            {
                Stream stream = File.Open(DATA_FILENAME, FileMode.Create);
                BinaryFormatter bFormatter = new BinaryFormatter();

                Console.WriteLine("Writing catches information.");
                bFormatter.Serialize(stream, this.listCatches);
                stream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("Unable to save catches information.");
            }
        }

        public void LoadCatches()
        {
            try
            {
                Stream stream = File.Open(DATA_FILENAME, FileMode.Open);
                BinaryFormatter bFormatter = new BinaryFormatter();

                Console.WriteLine("Reading catches information.");
                this.listCatches = (List<Catch>)bFormatter.Deserialize(stream);
                stream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("The file that contains catch information exists but there is a problem reading it.");
            }
        }

        public String Print()
        {
            if (this.listCatches.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Time, Type\n");
                foreach (Catch c in listCatches)
                {
                    sb.Append(c.Time + ", " + c.Species);
                    Console.WriteLine(c.Time + ", " + c.Species);
                }
                return sb.ToString();
            }
            else
            {
                Console.WriteLine("There are no saved information about catches.");
                return null;
            }
        }
    }
}
