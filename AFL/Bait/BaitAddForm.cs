﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace AFL
{
    public partial class BaitAddForm : Form
    {
        public BaitAddForm()
        {
            InitializeComponent();
            PopulateDropDown();
        }

        private void buttonUploadImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog chooseImage = new OpenFileDialog();
            chooseImage.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (chooseImage.ShowDialog() == DialogResult.OK)
            {
                pictureBoxImage.Image = new Bitmap(chooseImage.FileName);
            }
        }
        public Image Image { get; set; }
        public String Category { get; set; }
        public String Type { get; set; }
        public String Name { get; set; }
        public Color Color { get; set; }
        public String Brand { get; set; }
        public String Model { get; set; }
        public String Description { get; set; }
        public bool HasRecipe { get; set; }
        public String Recipe { get; set; }

        private BaitCategoryInformation baitCategoryInformation = new BaitCategoryInformation();

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            this.Image = pictureBoxImage.Image;
            this.Category = dropDownCategory.selectedValue.ToString();
            this.Type = dropDownType.selectedValue.ToString();
            this.Name = textBoxName.Text;
            this.Color = panelColor.BackColor;
            this.Brand = textBoxBrand.Text;
            this.Model = textBoxModel.Text;
            this.Description = richTextBoxDescription.Text;
            this.HasRecipe = checkBoxHasRecipe.Checked;
            this.Recipe = richTextBoxRecipe.Text;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panelColor_MouseClick(object sender, MouseEventArgs e)
        {
            ColorDialog colorDialog = new ColorDialog();
            colorDialog.ShowDialog();
            panelColor.BackColor = colorDialog.Color;
        }

        private void PopulateDropDown()
        {
            baitCategoryInformation.LoadBaitCategories();

            foreach(BaitCategory b in baitCategoryInformation.listBaitCategories)
            {
                dropDownCategory.AddItem(b.Name);
            }
            dropDownType.AddItem("Artificial");
            dropDownType.AddItem("Live");
            dropDownType.AddItem("Real");
        }
    }
}
