﻿using System;
using System.Drawing;
namespace AFL
{
    interface BaitInformationInterface
    {
        void AddBait(Image image, String category, String type, String name, Color color, String brand, String model, String description, bool hasRecipe, String recipe);
        void AddBait(String category, String type, String name, Color color, String brand, String model, String description, bool hasRecipe, String recipe);
        void RemoveBait(String category, String name);
        void SaveBait();
        void LoadBait();
    }
}
