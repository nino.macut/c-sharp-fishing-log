﻿using System;
using System.Drawing;
using System.Management.Instrumentation;

namespace AFL
{
    [Serializable]
    public class Bait
    {
        public Bait(Image Image, String Category, String Type, String Name, Color Color, String Brand, String Model, String Description, bool HasRecipe, String Recipe)
        {
            this.Image = Image;
            this.Category = Category;
            this.Type = Type;
            this.Name = Name;
            this.Color = Color;
            this.Brand = Brand;
            this.Model = Model;
            this.Description = Description;
            this.HasRecipe = HasRecipe;
            this.Recipe = Recipe;
        }

        public Image Image { get; set; }
        public String Category { get; set; }
        public String Type { get; set; }
        public String Name { get; set; }
        public Color Color { get; set; }
        public String Brand { get; set; }
        public String Model { get; set; }
        public String Description { get; set; }
        public bool HasRecipe { get; set; }
        public String Recipe { get; set; }

        public override String ToString()
        {
            return ("BAIT:\nCategory: " + Category + "\nType: " + Type + "\nName: " + Name);
        }
    }
}
