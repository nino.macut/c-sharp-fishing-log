﻿using AFL.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace AFL
{
    public partial class MainForm : Form
    {
        ListItemFisherman[] listItemFishermen = new ListItemFisherman[] { };
        FishermanInformation fishermanInformation = new FishermanInformation();
        public MainForm()
        {
            InitializeComponent();

            //Item Information Controllers.
            
            fishermanInformation.LoadFishermen();
            PopulateItems();
        }

        private void PopulateItems()
        {
            //POPULATE FISHERMEN!

            //Clear the fishermen FlowLayoutPanel.
            if (flpFishermen.Controls.Count > 0)
            {
                flpFishermen.Controls.Clear();
            }

            int index = -1;

            //Loop through each fisherman.
            foreach (Fisherman f in fishermanInformation.listFishermen)
            {
                index++;

                ListItemFisherman lif = new ListItemFisherman();
                lif.FirstName = f.FirstName;
                lif.LastName = f.LastName;
                lif.BirthDate = f.BirthDate;

                //Add button events
                lif.SetButtons();
                lif.DeleteButton.Click += delegate{
                    fishermanInformation.RemoveFisherman(lif.FirstName, lif.LastName);
                    fishermanInformation.SaveFishermen();
                    fishermanInformation.LoadFishermen();
                    PopulateItems();
                };

                lif.ShowValues();

                lif.EditButton.Click += delegate
                {
                    ShowEditFishermanDialogBox(index);
                };

                

                //Add to FlowLayoutPanel.
                flpFishermen.Controls.Add(lif);
            }
        }

        private void btnAddFisherman_Click(object sender, EventArgs e)
        {
            ShowAddFishermanDialogBox();
        }

        public void ShowAddFishermanDialogBox()
        {
            AddFishermanForm dialog = new AddFishermanForm();

            //Show AddFishermanForm as a modal dialog and determine if DialogResult = OK.

            //If dialog result is "OK" then add new fisherman informationand display it.
            if (dialog.ShowDialog(this) == DialogResult.OK)
            {
                fishermanInformation.AddFisherman(dialog.FirstName, dialog.LastName, dialog.BirthDate);
                fishermanInformation.SaveFishermen();
                fishermanInformation.LoadFishermen();
                PopulateItems();
            }
            else
            {
                Console.WriteLine("Canceled");
            }
            dialog.Dispose();
        }

        public void ShowEditFishermanDialogBox(int index)
        {
            EditFishermanForm dialog = new EditFishermanForm();
            Fisherman f = fishermanInformation.listFishermen[index];
            dialog.FirstName = f.FirstName;
            dialog.LastName = f.LastName;
            dialog.BirthDate = f.BirthDate;
            dialog.ShowValues();

            //Show EditFishermanForm as a modal dialog and determine if DialogResult = OK.

            //If dialog result is "OK" then edit fisherman information and display it.
            if (dialog.ShowDialog(this) == DialogResult.OK)
            {
                fishermanInformation.listFishermen[index].FirstName = dialog.FirstName;
                fishermanInformation.listFishermen[index].LastName = dialog.LastName;
                fishermanInformation.listFishermen[index].BirthDate = dialog.BirthDate;
                fishermanInformation.SaveFishermen();
                fishermanInformation.LoadFishermen();
                PopulateItems();
            }
            else
            {
                Console.WriteLine("Canceled");
            }
            dialog.Dispose();
        }

        private void lblFishermen_MouseClick(object sender, MouseEventArgs e)
        {
            lblFishermen.ForeColor = Color.FromName("White");
            pbFishermen.Image = Resources.fishermenOrange;
            pnlFishermen.Enabled = true;
            pnlFishermen.Visible = true;
        }

        private void lblFishermen_MouseHover(object sender, EventArgs e)
        {
            lblFishermen.ForeColor = Color.FromName("White");
            pbFishermen.Image = Resources.fishermenOrange;
        }
    }
}
