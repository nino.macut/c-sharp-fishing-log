﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AFL
{
    public partial class ListItemFisherman : UserControl
    {
        public ListItemFisherman()
        {
            InitializeComponent();
        }

        private String firstName;
        private String lastName;
        private DateTime birthDate;
        private Button btnDel;
        private Button btnEdt;
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
        public Button DeleteButton
        {
            get { return btnDel; }
            set { btnDel = value; }
        }

        public Button EditButton
        {
            get { return btnEdt; }
            set { btnEdt = value; }
        }

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public DateTime BirthDate
        {
            get { return birthDate; }
            set { birthDate = value; }
        }

        public void SetButtons()
        {
            DeleteButton = btnDelete;
            EditButton = btnEdit;
        }

        public void ShowValues()
        {
            lblFirstNameHere.Text = FirstName;
            lblLastNameHere.Text = LastName;
            lblBirthDateHere.Text = BirthDate.ToString();
        }
    }
}
