﻿namespace AFL
{
    partial class BaitListItemWithoutRecipe
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelNameHere = new System.Windows.Forms.Label();
            this.labelCategoryHere = new System.Windows.Forms.Label();
            this.labelColor = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.labelCategory = new System.Windows.Forms.Label();
            this.panelDetails2 = new System.Windows.Forms.Panel();
            this.panelColor = new System.Windows.Forms.Panel();
            this.labelBrand = new System.Windows.Forms.Label();
            this.labelModel = new System.Windows.Forms.Label();
            this.labelBrandHere = new System.Windows.Forms.Label();
            this.labelModelHere = new System.Windows.Forms.Label();
            this.labelDescription = new System.Windows.Forms.Label();
            this.labelType = new System.Windows.Forms.Label();
            this.labelTypeHere = new System.Windows.Forms.Label();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.pictureBoxImage = new System.Windows.Forms.PictureBox();
            this.richTextBoxDescriptionHere = new System.Windows.Forms.RichTextBox();
            this.panelDecoration = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // labelNameHere
            // 
            this.labelNameHere.AutoSize = true;
            this.labelNameHere.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNameHere.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelNameHere.Location = new System.Drawing.Point(199, 55);
            this.labelNameHere.Name = "labelNameHere";
            this.labelNameHere.Size = new System.Drawing.Size(103, 13);
            this.labelNameHere.TabIndex = 21;
            this.labelNameHere.Text = "Name goes here!";
            // 
            // labelCategoryHere
            // 
            this.labelCategoryHere.AutoSize = true;
            this.labelCategoryHere.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCategoryHere.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelCategoryHere.Location = new System.Drawing.Point(199, 15);
            this.labelCategoryHere.Name = "labelCategoryHere";
            this.labelCategoryHere.Size = new System.Drawing.Size(121, 13);
            this.labelCategoryHere.TabIndex = 20;
            this.labelCategoryHere.Text = "Category goes here!";
            // 
            // labelColor
            // 
            this.labelColor.AutoSize = true;
            this.labelColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelColor.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelColor.Location = new System.Drawing.Point(88, 75);
            this.labelColor.Name = "labelColor";
            this.labelColor.Size = new System.Drawing.Size(53, 13);
            this.labelColor.TabIndex = 19;
            this.labelColor.Text = "COLOR:";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelName.Location = new System.Drawing.Point(88, 55);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(46, 13);
            this.labelName.TabIndex = 18;
            this.labelName.Text = "NAME:";
            // 
            // labelCategory
            // 
            this.labelCategory.AutoSize = true;
            this.labelCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCategory.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelCategory.Location = new System.Drawing.Point(88, 15);
            this.labelCategory.Name = "labelCategory";
            this.labelCategory.Size = new System.Drawing.Size(78, 13);
            this.labelCategory.TabIndex = 17;
            this.labelCategory.Text = "CATEGORY:";
            // 
            // panelDetails2
            // 
            this.panelDetails2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(43)))), ((int)(((byte)(80)))));
            this.panelDetails2.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.panelDetails2.Location = new System.Drawing.Point(3, 73);
            this.panelDetails2.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.panelDetails2.Name = "panelDetails2";
            this.panelDetails2.Size = new System.Drawing.Size(70, 2);
            this.panelDetails2.TabIndex = 16;
            // 
            // panelColor
            // 
            this.panelColor.BackColor = System.Drawing.Color.White;
            this.panelColor.Location = new System.Drawing.Point(202, 75);
            this.panelColor.Name = "panelColor";
            this.panelColor.Size = new System.Drawing.Size(143, 13);
            this.panelColor.TabIndex = 25;
            // 
            // labelBrand
            // 
            this.labelBrand.AutoSize = true;
            this.labelBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBrand.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelBrand.Location = new System.Drawing.Point(88, 95);
            this.labelBrand.Name = "labelBrand";
            this.labelBrand.Size = new System.Drawing.Size(54, 13);
            this.labelBrand.TabIndex = 26;
            this.labelBrand.Text = "BRAND:";
            // 
            // labelModel
            // 
            this.labelModel.AutoSize = true;
            this.labelModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModel.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelModel.Location = new System.Drawing.Point(88, 115);
            this.labelModel.Name = "labelModel";
            this.labelModel.Size = new System.Drawing.Size(54, 13);
            this.labelModel.TabIndex = 27;
            this.labelModel.Text = "MODEL:";
            // 
            // labelBrandHere
            // 
            this.labelBrandHere.AutoSize = true;
            this.labelBrandHere.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBrandHere.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelBrandHere.Location = new System.Drawing.Point(199, 95);
            this.labelBrandHere.Name = "labelBrandHere";
            this.labelBrandHere.Size = new System.Drawing.Size(104, 13);
            this.labelBrandHere.TabIndex = 28;
            this.labelBrandHere.Text = "Brand goes here!";
            // 
            // labelModelHere
            // 
            this.labelModelHere.AutoSize = true;
            this.labelModelHere.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModelHere.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelModelHere.Location = new System.Drawing.Point(199, 115);
            this.labelModelHere.Name = "labelModelHere";
            this.labelModelHere.Size = new System.Drawing.Size(105, 13);
            this.labelModelHere.TabIndex = 29;
            this.labelModelHere.Text = "Model goes here!";
            // 
            // labelDescription
            // 
            this.labelDescription.AutoSize = true;
            this.labelDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDescription.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelDescription.Location = new System.Drawing.Point(88, 135);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(95, 13);
            this.labelDescription.TabIndex = 30;
            this.labelDescription.Text = "DESCRIPTION:";
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelType.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelType.Location = new System.Drawing.Point(88, 35);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(43, 13);
            this.labelType.TabIndex = 33;
            this.labelType.Text = "TYPE:";
            // 
            // labelTypeHere
            // 
            this.labelTypeHere.AutoSize = true;
            this.labelTypeHere.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTypeHere.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelTypeHere.Location = new System.Drawing.Point(199, 35);
            this.labelTypeHere.Name = "labelTypeHere";
            this.labelTypeHere.Size = new System.Drawing.Size(99, 13);
            this.labelTypeHere.TabIndex = 34;
            this.labelTypeHere.Text = "Type goes here!";
            // 
            // buttonDelete
            // 
            this.buttonDelete.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.buttonDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.buttonDelete.FlatAppearance.BorderSize = 0;
            this.buttonDelete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(23)))), ((int)(((byte)(38)))));
            this.buttonDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDelete.ForeColor = System.Drawing.Color.Gainsboro;
            this.buttonDelete.Image = global::AFL.Properties.Resources.deleteIconPink;
            this.buttonDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonDelete.Location = new System.Drawing.Point(375, 40);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(150, 40);
            this.buttonDelete.TabIndex = 24;
            this.buttonDelete.Text = "     DELETE";
            this.buttonDelete.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonDelete.UseVisualStyleBackColor = false;
            // 
            // buttonEdit
            // 
            this.buttonEdit.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.buttonEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.buttonEdit.FlatAppearance.BorderSize = 0;
            this.buttonEdit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(23)))), ((int)(((byte)(38)))));
            this.buttonEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEdit.ForeColor = System.Drawing.Color.Gainsboro;
            this.buttonEdit.Image = global::AFL.Properties.Resources.editIconPink;
            this.buttonEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonEdit.Location = new System.Drawing.Point(375, 0);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(150, 40);
            this.buttonEdit.TabIndex = 23;
            this.buttonEdit.Text = "     EDIT";
            this.buttonEdit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonEdit.UseVisualStyleBackColor = false;
            // 
            // pictureBoxImage
            // 
            this.pictureBoxImage.BackColor = System.Drawing.Color.Gainsboro;
            this.pictureBoxImage.Image = global::AFL.Properties.Resources.baits;
            this.pictureBoxImage.Location = new System.Drawing.Point(3, 5);
            this.pictureBoxImage.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.pictureBoxImage.Name = "pictureBoxImage";
            this.pictureBoxImage.Size = new System.Drawing.Size(70, 70);
            this.pictureBoxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxImage.TabIndex = 15;
            this.pictureBoxImage.TabStop = false;
            // 
            // richTextBoxDescriptionHere
            // 
            this.richTextBoxDescriptionHere.Location = new System.Drawing.Point(202, 135);
            this.richTextBoxDescriptionHere.Name = "richTextBoxDescriptionHere";
            this.richTextBoxDescriptionHere.Size = new System.Drawing.Size(143, 63);
            this.richTextBoxDescriptionHere.TabIndex = 51;
            this.richTextBoxDescriptionHere.Text = "";
            // 
            // panelDecoration
            // 
            this.panelDecoration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.panelDecoration.Location = new System.Drawing.Point(375, 80);
            this.panelDecoration.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.panelDecoration.Name = "panelDecoration";
            this.panelDecoration.Size = new System.Drawing.Size(150, 139);
            this.panelDecoration.TabIndex = 52;
            // 
            // BaitListItemWithoutRecipe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(20)))), ((int)(((byte)(34)))));
            this.Controls.Add(this.panelDecoration);
            this.Controls.Add(this.richTextBoxDescriptionHere);
            this.Controls.Add(this.labelTypeHere);
            this.Controls.Add(this.labelType);
            this.Controls.Add(this.labelDescription);
            this.Controls.Add(this.labelModelHere);
            this.Controls.Add(this.labelBrandHere);
            this.Controls.Add(this.labelModel);
            this.Controls.Add(this.labelBrand);
            this.Controls.Add(this.panelColor);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonEdit);
            this.Controls.Add(this.labelNameHere);
            this.Controls.Add(this.labelCategoryHere);
            this.Controls.Add(this.labelColor);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.labelCategory);
            this.Controls.Add(this.panelDetails2);
            this.Controls.Add(this.pictureBoxImage);
            this.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.Name = "BaitListItemWithoutRecipe";
            this.Size = new System.Drawing.Size(525, 214);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Label labelNameHere;
        private System.Windows.Forms.Label labelCategoryHere;
        private System.Windows.Forms.Label labelColor;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelCategory;
        private System.Windows.Forms.Panel panelDetails2;
        private System.Windows.Forms.PictureBox pictureBoxImage;
        private System.Windows.Forms.Panel panelColor;
        private System.Windows.Forms.Label labelBrand;
        private System.Windows.Forms.Label labelModel;
        private System.Windows.Forms.Label labelBrandHere;
        private System.Windows.Forms.Label labelModelHere;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.Label labelType;
        private System.Windows.Forms.Label labelTypeHere;
        private System.Windows.Forms.RichTextBox richTextBoxDescriptionHere;
        private System.Windows.Forms.Panel panelDecoration;
    }
}
