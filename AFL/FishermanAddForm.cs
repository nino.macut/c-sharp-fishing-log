﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AFL
{
    public partial class FishermanAddForm : Form
    {
        public FishermanAddForm()
        {
            InitializeComponent();
        }

        private void buttonUploadImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog chooseImage = new OpenFileDialog();
            chooseImage.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (chooseImage.ShowDialog() == DialogResult.OK)
            {
                pictureBoxProfile.Image = new Bitmap(chooseImage.FileName);
            }
        }

        public Image ProfilePicture { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }


        //On click set values and close form.
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            ProfilePicture = pictureBoxProfile.Image;
            FirstName = textBoxFirstName.Text;
            LastName = textBoxLastName.Text;
            BirthDate = dateTimePickerBirthDate.Value;
            this.Close();
        }

        //On click close form.
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
