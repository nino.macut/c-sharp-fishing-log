﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace AFL
{
    public partial class BaitEditForm : Form
    {
        public BaitEditForm()
        {
            InitializeComponent();
            PopulateDropDown();
        }

        public Image Image { get; set; }
        public String Category { get; set; }
        public String Type { get; set; }
        public String Name { get; set; }
        public Color Color { get; set; }
        public String Brand { get; set; }
        public String Model { get; set; }
        public String Description { get; set; }
        public bool HasRecipe { get; set; }
        public String Recipe { get; set; }

        private BaitCategoryInformation baitCategoryInformation = new BaitCategoryInformation();

        private void buttonUploadImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog chooseImage = new OpenFileDialog();
            chooseImage.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (chooseImage.ShowDialog() == DialogResult.OK)
            {
                pictureBoxImage.Image = new Bitmap(chooseImage.FileName);
            }
        }

        public void ShowValues()
        {
            pictureBoxImage.Image = this.Image;

            int selectedSpeciesIndex = 0;
            foreach (BaitCategory b in baitCategoryInformation.listBaitCategories)
            {
                if (b.Name == Category)
                {
                    break;
                }
                selectedSpeciesIndex++;
            }
            dropDownCategory.selectedIndex = selectedSpeciesIndex;

            textBoxName.Text = this.Name;
            panelColor.BackColor = this.Color;
            textBoxBrand.Text = this.Brand;
            textBoxModel.Text = this.Model;
            richTextBoxDescription.Text = this.Description;
            checkBoxHasRecipe.Checked = this.HasRecipe;
            richTextBoxRecipe.Text = this.Recipe;
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            this.Image = pictureBoxImage.Image;
            this.Category = dropDownCategory.selectedValue.ToString();
            this.Type = dropDownType.selectedValue.ToString();
            this.Name = textBoxName.Text;
            this.Color = panelColor.BackColor;
            this.Brand = textBoxBrand.Text;
            this.Model = textBoxModel.Text;
            this.Description = richTextBoxDescription.Text;
            this.HasRecipe = checkBoxHasRecipe.Checked;
            this.Recipe = richTextBoxRecipe.Text;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PopulateDropDown()
        {
            baitCategoryInformation.LoadBaitCategories();

            foreach (BaitCategory b in baitCategoryInformation.listBaitCategories)
            {
                dropDownCategory.AddItem(b.Name);
            }

            dropDownType.AddItem("Artificial");
            dropDownType.AddItem("Live");
            dropDownType.AddItem("Real");

            if (Type == "Artificial")
            {
                dropDownType.selectedIndex = 0;
            }
            else if (Type == "Live")
            {
                dropDownType.selectedIndex = 1;
            }
            else if (Type == "Real")
            {
                dropDownType.selectedIndex = 2;
            }
        }
    }
}
