﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AFL
{
    public partial class FishermanEditForm : Form
    {
        public FishermanEditForm()
        {
            InitializeComponent();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public Image ProfilePicture { get; set; }

        public void ShowValues()
        {
            textBoxFirstName.Text = FirstName;
            textBoxLastName.Text = LastName;
            dateTimePickerBirthDate.Value = BirthDate;
            pictureBoxProfile.Image = ProfilePicture;
        }

        //On click set values and close form.
        private void buttonEdit_Click(object sender, EventArgs e)
        {
            ProfilePicture = pictureBoxProfile.Image;
            FirstName = textBoxFirstName.Text;
            LastName = textBoxLastName.Text;
            BirthDate = dateTimePickerBirthDate.Value;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonUploadImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog chooseImage = new OpenFileDialog();
            chooseImage.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (chooseImage.ShowDialog() == DialogResult.OK)
            {
                pictureBoxProfile.Image = new Bitmap(chooseImage.FileName);
            }
        }
    }
}
