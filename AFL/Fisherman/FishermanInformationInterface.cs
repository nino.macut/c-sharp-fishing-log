﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFL
{
    interface FishermanInformationInterface
    {
        void AddFisherman(Image profilePicture, String firstName, String lastName, DateTime birthDate);
        void AddFisherman(String firstName, String lastName, DateTime birthDate);
        void RemoveFisherman(String firstName, String lastName);
        void SaveFishermen();
        void LoadFishermen();
    }
}
