﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace AFL
{
    public class FishermanInformation : FishermanInformationInterface
    {
        public List<Fisherman> listFishermen;

        //Fisherman information will be saved into this file.
        private const String DATA_FILENAME = "fishermenInformation.dat";

        public FishermanInformation()
        {
            this.listFishermen = new List<Fisherman>();
        }

        //Creates and adds a Fisherman object to listFishermen.
        public void AddFisherman(Image profilePicture, String firstName, String lastName, DateTime birthDate)
        {
            foreach (Fisherman f in listFishermen)
            {
                if (f.FirstName == firstName && f.LastName == lastName)
                {
                    Console.WriteLine("You had already added " + firstName + " " + lastName + " before."); //CHANGE LATER
                    return;
                }
            }
            this.listFishermen.Add(new Fisherman(profilePicture, firstName, lastName, birthDate));
            Console.WriteLine("Fisherman succesfully added.");
        }

        //Creates and adds a Fisherman object without an image to listFishermen.
        public void AddFisherman(String firstName, String lastName, DateTime birthDate)
        {
            foreach (Fisherman f in listFishermen)
            {
                if (f.FirstName == firstName && f.LastName == lastName)
                {
                    Console.WriteLine("You had already added " + firstName + " " + lastName + " before."); //CHANGE LATER
                    return;
                }
            }
            this.listFishermen.Add(new Fisherman(Properties.Resources.fishermen, firstName, lastName, birthDate));
            Console.WriteLine("Fisherman succesfully added.");
        }

        //Removes a specific Fisherman from listFishermen.
        public void RemoveFisherman(String firstName, String lastName)
        {
            foreach (Fisherman f in listFishermen)
            {
                if (f.FirstName == firstName && f.LastName == lastName)
                {
                    if (this.listFishermen.Remove(f))
                    {
                        Console.WriteLine(firstName + " " + lastName + " has been removed successfully."); //CHANGE LATER
                        return;
                    }
                    else
                    {
                        Console.WriteLine("Unable to remove " + firstName + " " + lastName); //CHANGE LATER
                        return;
                    }
                }
            }
            Console.WriteLine(firstName + " " + lastName + "had not been added before."); //CHANGE LATER
        }

        //Saves fisherman information into appropriate file.
        public void SaveFishermen()
        {
            try
            {
                Stream stream = File.Open(DATA_FILENAME, FileMode.Create);
                BinaryFormatter bFormatter = new BinaryFormatter();

                Console.WriteLine("Writing fishermen information.");
                bFormatter.Serialize(stream, this.listFishermen);
                stream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("Unable to save fishermen information.");
            }
        }

        //Loads fisherman information from appropriate file.
        public void LoadFishermen()
        {
            try
            {
                Stream stream = File.Open(DATA_FILENAME, FileMode.Open);
                BinaryFormatter bFormatter = new BinaryFormatter();

                Console.WriteLine("Reading fishermen information.");
                this.listFishermen = (List<Fisherman>)bFormatter.Deserialize(stream);
                stream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("The file that contains fishermen information exists but there is a problem reading it.");
            }
        }
    }
}