﻿namespace AFL
{
    partial class FormChildMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelFormControls = new System.Windows.Forms.Panel();
            this.buttonChildFormAdd = new System.Windows.Forms.Button();
            this.flowLayoutPanelForm = new System.Windows.Forms.FlowLayoutPanel();
            this.panelFormControls.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelFormControls
            // 
            this.panelFormControls.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(20)))), ((int)(((byte)(34)))));
            this.panelFormControls.Controls.Add(this.buttonChildFormAdd);
            this.panelFormControls.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelFormControls.Location = new System.Drawing.Point(0, 540);
            this.panelFormControls.Name = "panelFormControls";
            this.panelFormControls.Size = new System.Drawing.Size(700, 30);
            this.panelFormControls.TabIndex = 1;
            // 
            // buttonChildFormAdd
            // 
            this.buttonChildFormAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.buttonChildFormAdd.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonChildFormAdd.FlatAppearance.BorderSize = 0;
            this.buttonChildFormAdd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(23)))), ((int)(((byte)(38)))));
            this.buttonChildFormAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonChildFormAdd.ForeColor = System.Drawing.Color.LightGray;
            this.buttonChildFormAdd.Location = new System.Drawing.Point(550, 0);
            this.buttonChildFormAdd.Name = "buttonChildFormAdd";
            this.buttonChildFormAdd.Size = new System.Drawing.Size(150, 30);
            this.buttonChildFormAdd.TabIndex = 1;
            this.buttonChildFormAdd.Text = "ADD";
            this.buttonChildFormAdd.UseVisualStyleBackColor = false;
            // 
            // flowLayoutPanelForm
            // 
            this.flowLayoutPanelForm.AutoScroll = true;
            this.flowLayoutPanelForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(31)))), ((int)(((byte)(46)))));
            this.flowLayoutPanelForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelForm.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanelForm.Name = "flowLayoutPanelForm";
            this.flowLayoutPanelForm.Size = new System.Drawing.Size(700, 540);
            this.flowLayoutPanelForm.TabIndex = 2;
            // 
            // FormChildMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 570);
            this.Controls.Add(this.flowLayoutPanelForm);
            this.Controls.Add(this.panelFormControls);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormChildMain";
            this.Text = "FormChildMain";
            this.panelFormControls.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelFormControls;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelForm;
        private System.Windows.Forms.Button buttonChildFormAdd;
    }
}