﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFL
{
    public class CustomExceptions
    {
        //This exception is thrown when a selection of a drop down menu item is out of range.
        public class InvalidDropDownIndexException : Exception
        {
            public InvalidDropDownIndexException() { }

            public InvalidDropDownIndexException(int index) : base(String.Format("Invalid drop down index: ", index))
            {

            }
        }
    }
}
