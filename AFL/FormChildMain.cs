﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AFL
{
    public partial class FormChildMain : Form
    {
        public FormChildMain()
        {
            InitializeComponent();
            SetProperties();
        }

        public Button AddButton { get; set; }
        public FlowLayoutPanel ChildFlowLayoutPanel { get; set; }

        private void SetProperties()
        {
            AddButton = buttonChildFormAdd;
            ChildFlowLayoutPanel = flowLayoutPanelForm;
        }

        private void buttonCloseChildForm_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
