﻿using System;
using System.Drawing;

namespace AFL
{
    [Serializable]
    public class Catch
    {
        public Catch(DateTime Time, Image Image, String Species, String Location, String Bait, float WeatherTemperature, float WindSpeed, String WeatherConditions, float Length, float Weight, String WaterClarity, float Depth, float WaterTemperature, bool Method, String Note)
        {
            this.Time = Time;
            this.Image = Image;
            this.Species = Species;
            this.Location = Location;
            this.Bait = Bait;
            this.WeatherTemperature = WeatherTemperature;
            this.WindSpeed = WindSpeed;
            this.WeatherConditions = WeatherConditions;
            this.Length = Length;
            this.Weight = Weight;
            this.WaterClarity = WaterClarity;
            this.Depth = Depth;
            this.WaterTemperature = WaterTemperature;
            this.Method = Method;
            this.Note = Note;

        }
        public DateTime Time { get; set; }
        public Image Image { get; set; }
        public String Species { get; set; }
        public String Location { get; set; }
        public String Bait { get; set; }
        public float WeatherTemperature { get; set; }
        public float WindSpeed { get; set; }
        public String WeatherConditions { get; set; }
        public float Length { get; set; }
        public float Weight { get; set; }
        public String WaterClarity { get; set; }
        public float Depth { get; set; }
        public float WaterTemperature { get; set; }
        public bool Method { get; set; }
        public String Note { get; set; }
    }
}
