﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AFL
{
    public partial class BaitListItemWithRecipe : UserControl
    {
        public BaitListItemWithRecipe()
        {
            InitializeComponent();
            richTextBoxDescriptionHere.ReadOnly = true;
            richTextBoxRecipeHere.ReadOnly = true;
        }

        public Image Image { get; set; }
        public String Category { get; set; }
        public String Type { get; set; }
        public String Name { get; set; }
        public Color Color { get; set; }
        public String Brand { get; set; }
        public String Model { get; set; }
        public String Description { get; set; }
        public bool HasRecipe { get; set; }
        public String Recipe { get; set; }
        public Button DeleteButton { get; set; }
        public Button EditButton { get; set; }

        public void SetButtons()
        {
            DeleteButton = buttonDelete;
            EditButton = buttonEdit;
        }

        public void ShowValues()
        {
            pictureBoxImage.Image = Image;
            labelCategoryHere.Text = Category;
            labelTypeHere.Text = Type;
            labelNameHere.Text = Name;
            panelColor.BackColor = Color;
            labelBrandHere.Text = Brand;
            labelModelHere.Text = Model;
            richTextBoxDescriptionHere.Text = Description;
            richTextBoxRecipeHere.Text = Recipe;
        }
    }
}
