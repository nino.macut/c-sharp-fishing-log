﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AFL
{
    public partial class CatchAddForm : Form
    {
        public CatchAddForm()
        {
            InitializeComponent();
        }

        public SpeciesInformation speciesInformation = new SpeciesInformation();
        public BaitInformation baitInformation = new BaitInformation();
        public LocationInformation locationInformation = new LocationInformation();
        public Image Image { get; set; }
        public DateTime Time { get; set; }
        public String Species { get; set; }
        public String Location { get; set; }
        public String Bait { get; set; }
        public float WeatherTemperature { get; set; }
        public float WindSpeed { get; set; }
        public String WeatherConditions { get; set; }
        public float Length { get; set; }
        public float Weight { get; set; }
        public String WaterClarity { get; set; }
        public float Depth { get; set; }
        public float WaterTemperature { get; set; }
        public bool Method { get; set; }
        public String Note { get; set; }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            Image = pictureBoxImage.Image;
            Time = dateTimePickerTime.Value;
            Species = dropDownSpecies.selectedValue.ToString();
            Location = dropDownLocations.selectedValue.ToString();
            Bait = dropDownBait.selectedValue.ToString();
            WeatherTemperature = (float)Convert.ToDouble(textBoxWeatherTemperature.Text);
            WindSpeed = (float)Convert.ToDouble(textBoxWindSpeed.Text);
            WeatherConditions = richTextBoxWeatherConditions.Text;
            Length = (float)Convert.ToDouble(textBoxLength.Text);
            Weight = (float)Convert.ToDouble(textBoxWeight.Text);
            WaterClarity = textBoxWaterClarity.Text;
            Depth = (float)Convert.ToDouble(textBoxDepth.Text);
            WaterTemperature = (float)Convert.ToDouble(textBoxWaterTemperature.Text);
            Method = checkBoxMethod.Checked;
            Note = richTextBoxNote.Text;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonUploadImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog chooseImage = new OpenFileDialog();
            chooseImage.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (chooseImage.ShowDialog() == DialogResult.OK)
            {
                pictureBoxImage.Image = new Bitmap(chooseImage.FileName);
            }
        }

        public void LoadInformation()
        {
            speciesInformation.LoadSpecies();
            baitInformation.LoadBait();
            locationInformation.LoadLocations();
            foreach(Species s in speciesInformation.listSpecies)
            {
                dropDownSpecies.AddItem(s.Name);
            }
            foreach (Bait b in baitInformation.listBait)
            {
                dropDownBait.AddItem(b.Name);
            }
            foreach (Location l in locationInformation.listLocations)
            {
                dropDownLocations.AddItem(l.Name);
            }
        }
    }
}
