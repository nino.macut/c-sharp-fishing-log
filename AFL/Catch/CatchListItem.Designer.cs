﻿namespace AFL
{
    partial class CatchListItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CatchListItem));
            this.panelDetails2 = new System.Windows.Forms.Panel();
            this.buttonView = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.pictureBoxImage = new System.Windows.Forms.PictureBox();
            this.labelLocationHere = new System.Windows.Forms.Label();
            this.labelSpeciesHere = new System.Windows.Forms.Label();
            this.labelTimeHere = new System.Windows.Forms.Label();
            this.labelLocation = new System.Windows.Forms.Label();
            this.labelSpecies = new System.Windows.Forms.Label();
            this.labelTime = new System.Windows.Forms.Label();
            this.labelBaitHere = new System.Windows.Forms.Label();
            this.labelBait = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // panelDetails2
            // 
            this.panelDetails2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(43)))), ((int)(((byte)(80)))));
            this.panelDetails2.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.panelDetails2.Location = new System.Drawing.Point(6, 73);
            this.panelDetails2.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.panelDetails2.Name = "panelDetails2";
            this.panelDetails2.Size = new System.Drawing.Size(70, 2);
            this.panelDetails2.TabIndex = 6;
            // 
            // buttonView
            // 
            this.buttonView.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.buttonView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.buttonView.FlatAppearance.BorderSize = 0;
            this.buttonView.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(23)))), ((int)(((byte)(38)))));
            this.buttonView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonView.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonView.ForeColor = System.Drawing.Color.Gainsboro;
            this.buttonView.Image = global::AFL.Properties.Resources.viewIconPink;
            this.buttonView.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonView.Location = new System.Drawing.Point(368, 0);
            this.buttonView.Name = "buttonView";
            this.buttonView.Size = new System.Drawing.Size(150, 40);
            this.buttonView.TabIndex = 17;
            this.buttonView.Text = "     VIEW";
            this.buttonView.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonView.UseVisualStyleBackColor = false;
            // 
            // buttonDelete
            // 
            this.buttonDelete.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.buttonDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.buttonDelete.FlatAppearance.BorderSize = 0;
            this.buttonDelete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(23)))), ((int)(((byte)(38)))));
            this.buttonDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDelete.ForeColor = System.Drawing.Color.Gainsboro;
            this.buttonDelete.Image = global::AFL.Properties.Resources.deleteIconPink;
            this.buttonDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonDelete.Location = new System.Drawing.Point(368, 80);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(150, 40);
            this.buttonDelete.TabIndex = 16;
            this.buttonDelete.Text = "     DELETE";
            this.buttonDelete.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonDelete.UseVisualStyleBackColor = false;
            // 
            // buttonEdit
            // 
            this.buttonEdit.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.buttonEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.buttonEdit.FlatAppearance.BorderSize = 0;
            this.buttonEdit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(23)))), ((int)(((byte)(38)))));
            this.buttonEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEdit.ForeColor = System.Drawing.Color.Gainsboro;
            this.buttonEdit.Image = global::AFL.Properties.Resources.editIconPink;
            this.buttonEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonEdit.Location = new System.Drawing.Point(368, 40);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(150, 40);
            this.buttonEdit.TabIndex = 15;
            this.buttonEdit.Text = "     EDIT";
            this.buttonEdit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonEdit.UseVisualStyleBackColor = false;
            // 
            // pictureBoxImage
            // 
            this.pictureBoxImage.BackColor = System.Drawing.Color.Gainsboro;
            this.pictureBoxImage.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxImage.Image")));
            this.pictureBoxImage.Location = new System.Drawing.Point(6, 3);
            this.pictureBoxImage.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.pictureBoxImage.Name = "pictureBoxImage";
            this.pictureBoxImage.Size = new System.Drawing.Size(70, 70);
            this.pictureBoxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxImage.TabIndex = 5;
            this.pictureBoxImage.TabStop = false;
            // 
            // labelLocationHere
            // 
            this.labelLocationHere.AutoSize = true;
            this.labelLocationHere.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLocationHere.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelLocationHere.Location = new System.Drawing.Point(202, 55);
            this.labelLocationHere.Name = "labelLocationHere";
            this.labelLocationHere.Size = new System.Drawing.Size(120, 13);
            this.labelLocationHere.TabIndex = 23;
            this.labelLocationHere.Text = "Location goes here!";
            // 
            // labelSpeciesHere
            // 
            this.labelSpeciesHere.AutoSize = true;
            this.labelSpeciesHere.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSpeciesHere.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelSpeciesHere.Location = new System.Drawing.Point(202, 35);
            this.labelSpeciesHere.Name = "labelSpeciesHere";
            this.labelSpeciesHere.Size = new System.Drawing.Size(116, 13);
            this.labelSpeciesHere.TabIndex = 22;
            this.labelSpeciesHere.Text = "Species goes here!";
            // 
            // labelTimeHere
            // 
            this.labelTimeHere.AutoSize = true;
            this.labelTimeHere.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTimeHere.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelTimeHere.Location = new System.Drawing.Point(202, 15);
            this.labelTimeHere.Name = "labelTimeHere";
            this.labelTimeHere.Size = new System.Drawing.Size(98, 13);
            this.labelTimeHere.TabIndex = 21;
            this.labelTimeHere.Text = "Time goes here!";
            // 
            // labelLocation
            // 
            this.labelLocation.AutoSize = true;
            this.labelLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLocation.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelLocation.Location = new System.Drawing.Point(91, 55);
            this.labelLocation.Name = "labelLocation";
            this.labelLocation.Size = new System.Drawing.Size(73, 13);
            this.labelLocation.TabIndex = 20;
            this.labelLocation.Text = "LOCATION:";
            // 
            // labelSpecies
            // 
            this.labelSpecies.AutoSize = true;
            this.labelSpecies.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSpecies.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelSpecies.Location = new System.Drawing.Point(91, 35);
            this.labelSpecies.Name = "labelSpecies";
            this.labelSpecies.Size = new System.Drawing.Size(63, 13);
            this.labelSpecies.TabIndex = 19;
            this.labelSpecies.Text = "SPECIES:";
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTime.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelTime.Location = new System.Drawing.Point(91, 15);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(41, 13);
            this.labelTime.TabIndex = 18;
            this.labelTime.Text = "TIME:";
            // 
            // labelBaitHere
            // 
            this.labelBaitHere.AutoSize = true;
            this.labelBaitHere.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBaitHere.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelBaitHere.Location = new System.Drawing.Point(202, 75);
            this.labelBaitHere.Name = "labelBaitHere";
            this.labelBaitHere.Size = new System.Drawing.Size(93, 13);
            this.labelBaitHere.TabIndex = 26;
            this.labelBaitHere.Text = "Bait goes here!";
            // 
            // labelBait
            // 
            this.labelBait.AutoSize = true;
            this.labelBait.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBait.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelBait.Location = new System.Drawing.Point(91, 75);
            this.labelBait.Name = "labelBait";
            this.labelBait.Size = new System.Drawing.Size(39, 13);
            this.labelBait.TabIndex = 24;
            this.labelBait.Text = "BAIT:";
            // 
            // CatchListItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(20)))), ((int)(((byte)(34)))));
            this.Controls.Add(this.labelBaitHere);
            this.Controls.Add(this.labelBait);
            this.Controls.Add(this.labelLocationHere);
            this.Controls.Add(this.labelSpeciesHere);
            this.Controls.Add(this.labelTimeHere);
            this.Controls.Add(this.labelLocation);
            this.Controls.Add(this.labelSpecies);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.buttonView);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonEdit);
            this.Controls.Add(this.panelDetails2);
            this.Controls.Add(this.pictureBoxImage);
            this.Name = "CatchListItem";
            this.Size = new System.Drawing.Size(518, 120);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelDetails2;
        private System.Windows.Forms.PictureBox pictureBoxImage;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Button buttonView;
        private System.Windows.Forms.Label labelLocationHere;
        private System.Windows.Forms.Label labelSpeciesHere;
        private System.Windows.Forms.Label labelTimeHere;
        private System.Windows.Forms.Label labelLocation;
        private System.Windows.Forms.Label labelSpecies;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Label labelBaitHere;
        private System.Windows.Forms.Label labelBait;
    }
}
