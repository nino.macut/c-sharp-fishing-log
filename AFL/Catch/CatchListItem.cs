﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace AFL
{
    public partial class CatchListItem : UserControl
    {
        public CatchListItem()
        {
            InitializeComponent();
        }
        public DateTime Time { get; set; }
        public String Species { get; set; }
        public String Location { get; set; }
        public String Bait { get; set; }
        public Image Image { get; set; }
        public Button DeleteButton { get; set; }
        public Button EditButton { get; set; }
        public Button ViewButton { get; set; }

        public void SetButtons()
        {
            ViewButton = buttonView;
            DeleteButton = buttonDelete;
            EditButton = buttonEdit;
        }

        public void ShowValues()
        {
            pictureBoxImage.Image = Image;
            labelTimeHere.Text = Time.ToString("dd/MM/yyyy");
            labelSpeciesHere.Text = Species;
            labelLocationHere.Text = Location;
            labelBaitHere.Text = Bait;
        }
    }
}
