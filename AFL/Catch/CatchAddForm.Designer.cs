﻿namespace AFL
{
    partial class CatchAddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelTitle = new System.Windows.Forms.Panel();
            this.labelTitle = new System.Windows.Forms.Label();
            this.panelButtons = new System.Windows.Forms.Panel();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.panelDetail1 = new System.Windows.Forms.Panel();
            this.panelForm = new System.Windows.Forms.Panel();
            this.dropDownBait = new Bunifu.Framework.UI.BunifuDropdown();
            this.dropDownSpecies = new Bunifu.Framework.UI.BunifuDropdown();
            this.richTextBoxNote = new System.Windows.Forms.RichTextBox();
            this.labelNote = new System.Windows.Forms.Label();
            this.checkBoxMethod = new System.Windows.Forms.CheckBox();
            this.labelMethod = new System.Windows.Forms.Label();
            this.labelWaterTemperatureMeasure = new System.Windows.Forms.Label();
            this.textBoxWaterTemperature = new System.Windows.Forms.TextBox();
            this.labelWaterTemperature = new System.Windows.Forms.Label();
            this.labelDepthMeasure = new System.Windows.Forms.Label();
            this.textBoxDepth = new System.Windows.Forms.TextBox();
            this.labelDepth = new System.Windows.Forms.Label();
            this.labelWeightMeasure = new System.Windows.Forms.Label();
            this.labelLengthMeasure = new System.Windows.Forms.Label();
            this.textBoxWaterClarity = new System.Windows.Forms.TextBox();
            this.labelWaterClarity = new System.Windows.Forms.Label();
            this.textBoxWeight = new System.Windows.Forms.TextBox();
            this.textBoxLength = new System.Windows.Forms.TextBox();
            this.labelWeight = new System.Windows.Forms.Label();
            this.labelLength = new System.Windows.Forms.Label();
            this.richTextBoxWeatherConditions = new System.Windows.Forms.RichTextBox();
            this.labelWeatherTemperatureMeasure = new System.Windows.Forms.Label();
            this.labelWindSpeedMeasure = new System.Windows.Forms.Label();
            this.labelWeatherConditions = new System.Windows.Forms.Label();
            this.textBoxWindSpeed = new System.Windows.Forms.TextBox();
            this.textBoxWeatherTemperature = new System.Windows.Forms.TextBox();
            this.labelWindSpeed = new System.Windows.Forms.Label();
            this.labelWeatherTemperature = new System.Windows.Forms.Label();
            this.labelBait = new System.Windows.Forms.Label();
            this.dateTimePickerTime = new System.Windows.Forms.DateTimePicker();
            this.labelLocation = new System.Windows.Forms.Label();
            this.labelSpecies = new System.Windows.Forms.Label();
            this.labelTime = new System.Windows.Forms.Label();
            this.buttonUploadImage = new System.Windows.Forms.Button();
            this.panelDetails2 = new System.Windows.Forms.Panel();
            this.pictureBoxImage = new System.Windows.Forms.PictureBox();
            this.dropDownLocations = new Bunifu.Framework.UI.BunifuDropdown();
            this.panelTitle.SuspendLayout();
            this.panelButtons.SuspendLayout();
            this.panelForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // panelTitle
            // 
            this.panelTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.panelTitle.Controls.Add(this.labelTitle);
            this.panelTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitle.Location = new System.Drawing.Point(0, 0);
            this.panelTitle.Name = "panelTitle";
            this.panelTitle.Size = new System.Drawing.Size(600, 40);
            this.panelTitle.TabIndex = 1;
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelTitle.Location = new System.Drawing.Point(245, 10);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(111, 20);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "ADD CATCH";
            // 
            // panelButtons
            // 
            this.panelButtons.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.panelButtons.Controls.Add(this.buttonCancel);
            this.panelButtons.Controls.Add(this.buttonAdd);
            this.panelButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelButtons.Location = new System.Drawing.Point(0, 678);
            this.panelButtons.Name = "panelButtons";
            this.panelButtons.Size = new System.Drawing.Size(600, 40);
            this.panelButtons.TabIndex = 4;
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonCancel.FlatAppearance.BorderSize = 0;
            this.buttonCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(23)))), ((int)(((byte)(38)))));
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.ForeColor = System.Drawing.Color.Gainsboro;
            this.buttonCancel.Location = new System.Drawing.Point(240, 0);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(180, 40);
            this.buttonCancel.TabIndex = 22;
            this.buttonCancel.Text = "CANCEL";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.buttonAdd.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.buttonAdd.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonAdd.FlatAppearance.BorderSize = 0;
            this.buttonAdd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(23)))), ((int)(((byte)(38)))));
            this.buttonAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdd.ForeColor = System.Drawing.Color.Gainsboro;
            this.buttonAdd.Location = new System.Drawing.Point(420, 0);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(180, 40);
            this.buttonAdd.TabIndex = 21;
            this.buttonAdd.Text = "ADD";
            this.buttonAdd.UseVisualStyleBackColor = false;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // panelDetail1
            // 
            this.panelDetail1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(43)))), ((int)(((byte)(80)))));
            this.panelDetail1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelDetail1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.panelDetail1.Location = new System.Drawing.Point(0, 718);
            this.panelDetail1.Name = "panelDetail1";
            this.panelDetail1.Size = new System.Drawing.Size(600, 2);
            this.panelDetail1.TabIndex = 3;
            // 
            // panelForm
            // 
            this.panelForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(20)))), ((int)(((byte)(34)))));
            this.panelForm.Controls.Add(this.dropDownLocations);
            this.panelForm.Controls.Add(this.dropDownBait);
            this.panelForm.Controls.Add(this.dropDownSpecies);
            this.panelForm.Controls.Add(this.richTextBoxNote);
            this.panelForm.Controls.Add(this.labelNote);
            this.panelForm.Controls.Add(this.checkBoxMethod);
            this.panelForm.Controls.Add(this.labelMethod);
            this.panelForm.Controls.Add(this.labelWaterTemperatureMeasure);
            this.panelForm.Controls.Add(this.textBoxWaterTemperature);
            this.panelForm.Controls.Add(this.labelWaterTemperature);
            this.panelForm.Controls.Add(this.labelDepthMeasure);
            this.panelForm.Controls.Add(this.textBoxDepth);
            this.panelForm.Controls.Add(this.labelDepth);
            this.panelForm.Controls.Add(this.labelWeightMeasure);
            this.panelForm.Controls.Add(this.labelLengthMeasure);
            this.panelForm.Controls.Add(this.textBoxWaterClarity);
            this.panelForm.Controls.Add(this.labelWaterClarity);
            this.panelForm.Controls.Add(this.textBoxWeight);
            this.panelForm.Controls.Add(this.textBoxLength);
            this.panelForm.Controls.Add(this.labelWeight);
            this.panelForm.Controls.Add(this.labelLength);
            this.panelForm.Controls.Add(this.richTextBoxWeatherConditions);
            this.panelForm.Controls.Add(this.labelWeatherTemperatureMeasure);
            this.panelForm.Controls.Add(this.labelWindSpeedMeasure);
            this.panelForm.Controls.Add(this.labelWeatherConditions);
            this.panelForm.Controls.Add(this.textBoxWindSpeed);
            this.panelForm.Controls.Add(this.textBoxWeatherTemperature);
            this.panelForm.Controls.Add(this.labelWindSpeed);
            this.panelForm.Controls.Add(this.labelWeatherTemperature);
            this.panelForm.Controls.Add(this.labelBait);
            this.panelForm.Controls.Add(this.dateTimePickerTime);
            this.panelForm.Controls.Add(this.labelLocation);
            this.panelForm.Controls.Add(this.labelSpecies);
            this.panelForm.Controls.Add(this.labelTime);
            this.panelForm.Controls.Add(this.buttonUploadImage);
            this.panelForm.Controls.Add(this.panelDetails2);
            this.panelForm.Controls.Add(this.pictureBoxImage);
            this.panelForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelForm.Location = new System.Drawing.Point(0, 40);
            this.panelForm.Name = "panelForm";
            this.panelForm.Size = new System.Drawing.Size(600, 638);
            this.panelForm.TabIndex = 5;
            // 
            // dropDownBait
            // 
            this.dropDownBait.BackColor = System.Drawing.Color.Transparent;
            this.dropDownBait.BorderRadius = 3;
            this.dropDownBait.ForeColor = System.Drawing.Color.Black;
            this.dropDownBait.Items = new string[0];
            this.dropDownBait.Location = new System.Drawing.Point(314, 210);
            this.dropDownBait.Name = "dropDownBait";
            this.dropDownBait.NomalColor = System.Drawing.SystemColors.Window;
            this.dropDownBait.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(43)))), ((int)(((byte)(80)))));
            this.dropDownBait.selectedIndex = -1;
            this.dropDownBait.Size = new System.Drawing.Size(187, 21);
            this.dropDownBait.TabIndex = 10;
            // 
            // dropDownSpecies
            // 
            this.dropDownSpecies.BackColor = System.Drawing.Color.Transparent;
            this.dropDownSpecies.BorderRadius = 3;
            this.dropDownSpecies.ForeColor = System.Drawing.Color.Black;
            this.dropDownSpecies.Items = new string[0];
            this.dropDownSpecies.Location = new System.Drawing.Point(314, 150);
            this.dropDownSpecies.Name = "dropDownSpecies";
            this.dropDownSpecies.NomalColor = System.Drawing.SystemColors.Window;
            this.dropDownSpecies.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(43)))), ((int)(((byte)(80)))));
            this.dropDownSpecies.selectedIndex = -1;
            this.dropDownSpecies.Size = new System.Drawing.Size(187, 21);
            this.dropDownSpecies.TabIndex = 8;
            // 
            // richTextBoxNote
            // 
            this.richTextBoxNote.Location = new System.Drawing.Point(314, 550);
            this.richTextBoxNote.Name = "richTextBoxNote";
            this.richTextBoxNote.Size = new System.Drawing.Size(187, 58);
            this.richTextBoxNote.TabIndex = 20;
            this.richTextBoxNote.Text = "";
            // 
            // labelNote
            // 
            this.labelNote.AutoSize = true;
            this.labelNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNote.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelNote.Location = new System.Drawing.Point(70, 550);
            this.labelNote.Name = "labelNote";
            this.labelNote.Size = new System.Drawing.Size(56, 17);
            this.labelNote.TabIndex = 37;
            this.labelNote.Text = "NOTE:";
            // 
            // checkBoxMethod
            // 
            this.checkBoxMethod.AutoSize = true;
            this.checkBoxMethod.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.checkBoxMethod.ForeColor = System.Drawing.Color.Gainsboro;
            this.checkBoxMethod.Location = new System.Drawing.Point(314, 520);
            this.checkBoxMethod.Name = "checkBoxMethod";
            this.checkBoxMethod.Size = new System.Drawing.Size(15, 14);
            this.checkBoxMethod.TabIndex = 19;
            this.checkBoxMethod.UseVisualStyleBackColor = true;
            // 
            // labelMethod
            // 
            this.labelMethod.AutoSize = true;
            this.labelMethod.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMethod.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelMethod.Location = new System.Drawing.Point(71, 520);
            this.labelMethod.Name = "labelMethod";
            this.labelMethod.Size = new System.Drawing.Size(94, 17);
            this.labelMethod.TabIndex = 35;
            this.labelMethod.Text = "RELEASED:";
            // 
            // labelWaterTemperatureMeasure
            // 
            this.labelWaterTemperatureMeasure.AutoSize = true;
            this.labelWaterTemperatureMeasure.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWaterTemperatureMeasure.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelWaterTemperatureMeasure.Location = new System.Drawing.Point(507, 493);
            this.labelWaterTemperatureMeasure.Name = "labelWaterTemperatureMeasure";
            this.labelWaterTemperatureMeasure.Size = new System.Drawing.Size(25, 17);
            this.labelWaterTemperatureMeasure.TabIndex = 34;
            this.labelWaterTemperatureMeasure.Text = "°C";
            // 
            // textBoxWaterTemperature
            // 
            this.textBoxWaterTemperature.Location = new System.Drawing.Point(314, 490);
            this.textBoxWaterTemperature.Name = "textBoxWaterTemperature";
            this.textBoxWaterTemperature.Size = new System.Drawing.Size(187, 20);
            this.textBoxWaterTemperature.TabIndex = 18;
            // 
            // labelWaterTemperature
            // 
            this.labelWaterTemperature.AutoSize = true;
            this.labelWaterTemperature.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWaterTemperature.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelWaterTemperature.Location = new System.Drawing.Point(70, 490);
            this.labelWaterTemperature.Name = "labelWaterTemperature";
            this.labelWaterTemperature.Size = new System.Drawing.Size(188, 17);
            this.labelWaterTemperature.TabIndex = 32;
            this.labelWaterTemperature.Text = "WATER TEMPERATURE:";
            // 
            // labelDepthMeasure
            // 
            this.labelDepthMeasure.AutoSize = true;
            this.labelDepthMeasure.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDepthMeasure.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelDepthMeasure.Location = new System.Drawing.Point(507, 460);
            this.labelDepthMeasure.Name = "labelDepthMeasure";
            this.labelDepthMeasure.Size = new System.Drawing.Size(20, 17);
            this.labelDepthMeasure.TabIndex = 31;
            this.labelDepthMeasure.Text = "M";
            // 
            // textBoxDepth
            // 
            this.textBoxDepth.Location = new System.Drawing.Point(314, 460);
            this.textBoxDepth.Name = "textBoxDepth";
            this.textBoxDepth.Size = new System.Drawing.Size(187, 20);
            this.textBoxDepth.TabIndex = 17;
            // 
            // labelDepth
            // 
            this.labelDepth.AutoSize = true;
            this.labelDepth.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDepth.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelDepth.Location = new System.Drawing.Point(70, 460);
            this.labelDepth.Name = "labelDepth";
            this.labelDepth.Size = new System.Drawing.Size(65, 17);
            this.labelDepth.TabIndex = 29;
            this.labelDepth.Text = "DEPTH:";
            // 
            // labelWeightMeasure
            // 
            this.labelWeightMeasure.AutoSize = true;
            this.labelWeightMeasure.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWeightMeasure.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelWeightMeasure.Location = new System.Drawing.Point(507, 400);
            this.labelWeightMeasure.Name = "labelWeightMeasure";
            this.labelWeightMeasure.Size = new System.Drawing.Size(30, 17);
            this.labelWeightMeasure.TabIndex = 28;
            this.labelWeightMeasure.Text = "KG";
            // 
            // labelLengthMeasure
            // 
            this.labelLengthMeasure.AutoSize = true;
            this.labelLengthMeasure.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLengthMeasure.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelLengthMeasure.Location = new System.Drawing.Point(507, 371);
            this.labelLengthMeasure.Name = "labelLengthMeasure";
            this.labelLengthMeasure.Size = new System.Drawing.Size(30, 17);
            this.labelLengthMeasure.TabIndex = 27;
            this.labelLengthMeasure.Text = "CM";
            // 
            // textBoxWaterClarity
            // 
            this.textBoxWaterClarity.Location = new System.Drawing.Point(314, 430);
            this.textBoxWaterClarity.Name = "textBoxWaterClarity";
            this.textBoxWaterClarity.Size = new System.Drawing.Size(187, 20);
            this.textBoxWaterClarity.TabIndex = 16;
            // 
            // labelWaterClarity
            // 
            this.labelWaterClarity.AutoSize = true;
            this.labelWaterClarity.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWaterClarity.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelWaterClarity.Location = new System.Drawing.Point(70, 430);
            this.labelWaterClarity.Name = "labelWaterClarity";
            this.labelWaterClarity.Size = new System.Drawing.Size(137, 17);
            this.labelWaterClarity.TabIndex = 25;
            this.labelWaterClarity.Text = "WATER CLARITY:";
            // 
            // textBoxWeight
            // 
            this.textBoxWeight.Location = new System.Drawing.Point(314, 400);
            this.textBoxWeight.Name = "textBoxWeight";
            this.textBoxWeight.Size = new System.Drawing.Size(187, 20);
            this.textBoxWeight.TabIndex = 15;
            // 
            // textBoxLength
            // 
            this.textBoxLength.Location = new System.Drawing.Point(314, 370);
            this.textBoxLength.Name = "textBoxLength";
            this.textBoxLength.Size = new System.Drawing.Size(187, 20);
            this.textBoxLength.TabIndex = 14;
            // 
            // labelWeight
            // 
            this.labelWeight.AutoSize = true;
            this.labelWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWeight.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelWeight.Location = new System.Drawing.Point(70, 400);
            this.labelWeight.Name = "labelWeight";
            this.labelWeight.Size = new System.Drawing.Size(74, 17);
            this.labelWeight.TabIndex = 22;
            this.labelWeight.Text = "WEIGHT:";
            // 
            // labelLength
            // 
            this.labelLength.AutoSize = true;
            this.labelLength.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLength.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelLength.Location = new System.Drawing.Point(70, 370);
            this.labelLength.Name = "labelLength";
            this.labelLength.Size = new System.Drawing.Size(76, 17);
            this.labelLength.TabIndex = 21;
            this.labelLength.Text = "LENGTH:";
            // 
            // richTextBoxWeatherConditions
            // 
            this.richTextBoxWeatherConditions.Location = new System.Drawing.Point(314, 302);
            this.richTextBoxWeatherConditions.Name = "richTextBoxWeatherConditions";
            this.richTextBoxWeatherConditions.Size = new System.Drawing.Size(187, 58);
            this.richTextBoxWeatherConditions.TabIndex = 13;
            this.richTextBoxWeatherConditions.Text = "";
            // 
            // labelWeatherTemperatureMeasure
            // 
            this.labelWeatherTemperatureMeasure.AutoSize = true;
            this.labelWeatherTemperatureMeasure.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWeatherTemperatureMeasure.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelWeatherTemperatureMeasure.Location = new System.Drawing.Point(507, 243);
            this.labelWeatherTemperatureMeasure.Name = "labelWeatherTemperatureMeasure";
            this.labelWeatherTemperatureMeasure.Size = new System.Drawing.Size(25, 17);
            this.labelWeatherTemperatureMeasure.TabIndex = 19;
            this.labelWeatherTemperatureMeasure.Text = "°C";
            // 
            // labelWindSpeedMeasure
            // 
            this.labelWindSpeedMeasure.AutoSize = true;
            this.labelWindSpeedMeasure.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWindSpeedMeasure.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelWindSpeedMeasure.Location = new System.Drawing.Point(507, 272);
            this.labelWindSpeedMeasure.Name = "labelWindSpeedMeasure";
            this.labelWindSpeedMeasure.Size = new System.Drawing.Size(46, 17);
            this.labelWindSpeedMeasure.TabIndex = 18;
            this.labelWindSpeedMeasure.Text = "KM/H";
            // 
            // labelWeatherConditions
            // 
            this.labelWeatherConditions.AutoSize = true;
            this.labelWeatherConditions.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWeatherConditions.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelWeatherConditions.Location = new System.Drawing.Point(70, 300);
            this.labelWeatherConditions.Name = "labelWeatherConditions";
            this.labelWeatherConditions.Size = new System.Drawing.Size(189, 17);
            this.labelWeatherConditions.TabIndex = 16;
            this.labelWeatherConditions.Text = "WEATHER CONDITIONS:";
            // 
            // textBoxWindSpeed
            // 
            this.textBoxWindSpeed.Location = new System.Drawing.Point(314, 272);
            this.textBoxWindSpeed.Name = "textBoxWindSpeed";
            this.textBoxWindSpeed.Size = new System.Drawing.Size(187, 20);
            this.textBoxWindSpeed.TabIndex = 12;
            // 
            // textBoxWeatherTemperature
            // 
            this.textBoxWeatherTemperature.Location = new System.Drawing.Point(314, 242);
            this.textBoxWeatherTemperature.Name = "textBoxWeatherTemperature";
            this.textBoxWeatherTemperature.Size = new System.Drawing.Size(187, 20);
            this.textBoxWeatherTemperature.TabIndex = 11;
            // 
            // labelWindSpeed
            // 
            this.labelWindSpeed.AutoSize = true;
            this.labelWindSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWindSpeed.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelWindSpeed.Location = new System.Drawing.Point(70, 270);
            this.labelWindSpeed.Name = "labelWindSpeed";
            this.labelWindSpeed.Size = new System.Drawing.Size(109, 17);
            this.labelWindSpeed.TabIndex = 13;
            this.labelWindSpeed.Text = "WIND SPEED:";
            // 
            // labelWeatherTemperature
            // 
            this.labelWeatherTemperature.AutoSize = true;
            this.labelWeatherTemperature.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWeatherTemperature.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelWeatherTemperature.Location = new System.Drawing.Point(70, 240);
            this.labelWeatherTemperature.Name = "labelWeatherTemperature";
            this.labelWeatherTemperature.Size = new System.Drawing.Size(209, 17);
            this.labelWeatherTemperature.TabIndex = 12;
            this.labelWeatherTemperature.Text = "WEATHER TEMPERATURE:";
            // 
            // labelBait
            // 
            this.labelBait.AutoSize = true;
            this.labelBait.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBait.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelBait.Location = new System.Drawing.Point(70, 210);
            this.labelBait.Name = "labelBait";
            this.labelBait.Size = new System.Drawing.Size(47, 17);
            this.labelBait.TabIndex = 10;
            this.labelBait.Text = "BAIT:";
            // 
            // dateTimePickerTime
            // 
            this.dateTimePickerTime.Location = new System.Drawing.Point(314, 120);
            this.dateTimePickerTime.Name = "dateTimePickerTime";
            this.dateTimePickerTime.Size = new System.Drawing.Size(187, 20);
            this.dateTimePickerTime.TabIndex = 7;
            // 
            // labelLocation
            // 
            this.labelLocation.AutoSize = true;
            this.labelLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLocation.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelLocation.Location = new System.Drawing.Point(70, 180);
            this.labelLocation.Name = "labelLocation";
            this.labelLocation.Size = new System.Drawing.Size(91, 17);
            this.labelLocation.TabIndex = 6;
            this.labelLocation.Text = "LOCATION:";
            // 
            // labelSpecies
            // 
            this.labelSpecies.AutoSize = true;
            this.labelSpecies.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSpecies.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelSpecies.Location = new System.Drawing.Point(70, 150);
            this.labelSpecies.Name = "labelSpecies";
            this.labelSpecies.Size = new System.Drawing.Size(77, 17);
            this.labelSpecies.TabIndex = 5;
            this.labelSpecies.Text = "SPECIES:";
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTime.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelTime.Location = new System.Drawing.Point(70, 120);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(54, 17);
            this.labelTime.TabIndex = 4;
            this.labelTime.Text = "DATE:";
            // 
            // buttonUploadImage
            // 
            this.buttonUploadImage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.buttonUploadImage.FlatAppearance.BorderSize = 0;
            this.buttonUploadImage.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(23)))), ((int)(((byte)(38)))));
            this.buttonUploadImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUploadImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUploadImage.ForeColor = System.Drawing.Color.Gainsboro;
            this.buttonUploadImage.Location = new System.Drawing.Point(195, 55);
            this.buttonUploadImage.Name = "buttonUploadImage";
            this.buttonUploadImage.Size = new System.Drawing.Size(187, 20);
            this.buttonUploadImage.TabIndex = 3;
            this.buttonUploadImage.Text = "UPLOAD IMAGE";
            this.buttonUploadImage.UseVisualStyleBackColor = false;
            this.buttonUploadImage.Click += new System.EventHandler(this.buttonUploadImage_Click);
            // 
            // panelDetails2
            // 
            this.panelDetails2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(43)))), ((int)(((byte)(80)))));
            this.panelDetails2.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.panelDetails2.Location = new System.Drawing.Point(105, 101);
            this.panelDetails2.Name = "panelDetails2";
            this.panelDetails2.Size = new System.Drawing.Size(70, 2);
            this.panelDetails2.TabIndex = 2;
            // 
            // pictureBoxImage
            // 
            this.pictureBoxImage.BackColor = System.Drawing.Color.Gainsboro;
            this.pictureBoxImage.Image = global::AFL.Properties.Resources.catches;
            this.pictureBoxImage.Location = new System.Drawing.Point(105, 33);
            this.pictureBoxImage.Name = "pictureBoxImage";
            this.pictureBoxImage.Size = new System.Drawing.Size(70, 70);
            this.pictureBoxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxImage.TabIndex = 0;
            this.pictureBoxImage.TabStop = false;
            // 
            // dropDownLocations
            // 
            this.dropDownLocations.BackColor = System.Drawing.Color.Transparent;
            this.dropDownLocations.BorderRadius = 3;
            this.dropDownLocations.ForeColor = System.Drawing.Color.Black;
            this.dropDownLocations.Items = new string[0];
            this.dropDownLocations.Location = new System.Drawing.Point(314, 180);
            this.dropDownLocations.Name = "dropDownLocations";
            this.dropDownLocations.NomalColor = System.Drawing.SystemColors.Window;
            this.dropDownLocations.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(43)))), ((int)(((byte)(80)))));
            this.dropDownLocations.selectedIndex = -1;
            this.dropDownLocations.Size = new System.Drawing.Size(187, 21);
            this.dropDownLocations.TabIndex = 9;
            // 
            // CatchAddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 720);
            this.Controls.Add(this.panelForm);
            this.Controls.Add(this.panelButtons);
            this.Controls.Add(this.panelDetail1);
            this.Controls.Add(this.panelTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CatchAddForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "CatchAddForm";
            this.panelTitle.ResumeLayout(false);
            this.panelTitle.PerformLayout();
            this.panelButtons.ResumeLayout(false);
            this.panelForm.ResumeLayout(false);
            this.panelForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTitle;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Panel panelButtons;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Panel panelDetail1;
        private System.Windows.Forms.Panel panelForm;
        private System.Windows.Forms.DateTimePicker dateTimePickerTime;
        private System.Windows.Forms.Label labelLocation;
        private System.Windows.Forms.Label labelSpecies;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Button buttonUploadImage;
        private System.Windows.Forms.Panel panelDetails2;
        private System.Windows.Forms.PictureBox pictureBoxImage;
        private System.Windows.Forms.Label labelBait;
        private System.Windows.Forms.Label labelWeatherConditions;
        private System.Windows.Forms.TextBox textBoxWindSpeed;
        private System.Windows.Forms.Label labelWindSpeed;
        private System.Windows.Forms.Label labelWeatherTemperature;
        private System.Windows.Forms.TextBox textBoxWeatherTemperature;
        private System.Windows.Forms.Label labelWeatherTemperatureMeasure;
        private System.Windows.Forms.Label labelWindSpeedMeasure;
        private System.Windows.Forms.RichTextBox richTextBoxWeatherConditions;
        private System.Windows.Forms.Label labelWaterClarity;
        private System.Windows.Forms.TextBox textBoxWeight;
        private System.Windows.Forms.TextBox textBoxLength;
        private System.Windows.Forms.Label labelWeight;
        private System.Windows.Forms.Label labelLength;
        private System.Windows.Forms.Label labelLengthMeasure;
        private System.Windows.Forms.TextBox textBoxWaterClarity;
        private System.Windows.Forms.Label labelWeightMeasure;
        private System.Windows.Forms.TextBox textBoxDepth;
        private System.Windows.Forms.Label labelDepth;
        private System.Windows.Forms.Label labelDepthMeasure;
        private System.Windows.Forms.TextBox textBoxWaterTemperature;
        private System.Windows.Forms.Label labelWaterTemperature;
        private System.Windows.Forms.Label labelWaterTemperatureMeasure;
        private System.Windows.Forms.CheckBox checkBoxMethod;
        private System.Windows.Forms.Label labelMethod;
        private System.Windows.Forms.RichTextBox richTextBoxNote;
        private System.Windows.Forms.Label labelNote;
        private Bunifu.Framework.UI.BunifuDropdown dropDownSpecies;
        private Bunifu.Framework.UI.BunifuDropdown dropDownBait;
        private Bunifu.Framework.UI.BunifuDropdown dropDownLocations;
    }
}