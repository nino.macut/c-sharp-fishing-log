﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFL
{
    interface CatchInformationInterface
    {
        void AddCatch(DateTime Time, Image Image, String Species, String Location, String Bait, float WeatherTemperature, float WindSpeed, String WeatherConditions, float Length, float Weight, String WaterClarity, float Depth, float WaterTemperature, bool Method, String Note);
        void AddCatch(DateTime Time, String Species, String Location, String Bait, float WeatherTemperature, float WindSpeed, String WeatherConditions, float Length, float Weight, String WaterClarity, float Depth, float WaterTemperature, bool Method, String Note);
        void RemoveCatch(DateTime time, String species);
        void SaveCatches();
        void LoadCatches();
    }
}
