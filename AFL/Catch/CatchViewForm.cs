﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AFL
{
    public partial class CatchViewForm : Form
    {
        public CatchViewForm()
        {
            InitializeComponent();
            dateTimePickerTime.Enabled = false;
            textBoxSpecies.ReadOnly = true;
            textBoxLocation.ReadOnly = true;
            textBoxBait.ReadOnly = true;
            textBoxWeatherTemperature.ReadOnly = true;
            textBoxWindSpeed.ReadOnly = true;
            richTextBoxWeatherConditions.ReadOnly = true;
            textBoxLength.ReadOnly = true;
            textBoxWeight.ReadOnly = true;
            textBoxWaterClarity.ReadOnly = true;
            textBoxDepth.ReadOnly = true;
            textBoxWaterTemperature.ReadOnly = true;
            checkBoxMethod.Enabled = false;
            richTextBoxNote.ReadOnly = true;
        }

        public Image Image { get; set; }
        public DateTime Time { get; set; }
        public String Species { get; set; }
        public String Location { get; set; }
        public String Bait { get; set; }
        public float WeatherTemperature { get; set; }
        public float WindSpeed { get; set; }
        public String WeatherConditions { get; set; }
        public float Length { get; set; }
        public float Weight { get; set; }
        public String WaterClarity { get; set; }
        public float Depth { get; set; }
        public float WaterTemperature { get; set; }
        public bool Method { get; set; }
        public String Note { get; set; }

        public void ShowValues()
        {
            pictureBoxImage.Image = Image;
            dateTimePickerTime.Value = Time;
            textBoxSpecies.Text = Species;
            textBoxLocation.Text = Location;
            textBoxBait.Text = Bait;
            textBoxWeatherTemperature.Text = WeatherTemperature.ToString();
            textBoxWindSpeed.Text = WindSpeed.ToString();
            richTextBoxWeatherConditions.Text = WeatherConditions;
            textBoxLength.Text = Length.ToString();
            textBoxWeight.Text = Weight.ToString();
            textBoxWaterClarity.Text = WaterClarity;
            textBoxDepth.Text = Depth.ToString();
            textBoxWaterTemperature.Text = WaterTemperature.ToString();
            checkBoxMethod.Checked = Method;
            richTextBoxNote.Text = Note;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
