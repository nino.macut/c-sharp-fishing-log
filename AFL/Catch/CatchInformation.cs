﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace AFL
{
    public class CatchInformation : CatchInformationInterface
    {
        public List<Catch> listCatches;

        //Catch information will be saved to this file.
        private const String DATA_FILENAME = "catchInformation.dat";

        public CatchInformation()
        {
            this.listCatches = new List<Catch>();
        }

        //Creates and adds a Catch object to listCatches.
        public void AddCatch(DateTime Time, Image Image, String Species, String Location, String Bait, float WeatherTemperature, float WindSpeed, String WeatherConditions, float Length, float Weight, String WaterClarity, float Depth, float WaterTemperature, bool Method, String Note)
        {
            this.listCatches.Add(new Catch(Time, Image, Species, Location, Bait, WeatherTemperature, WindSpeed, WeatherConditions, Length, Weight, WaterClarity, Depth, WaterTemperature, Method, Note));
            Console.WriteLine("Catch succesfully added.");
        }

        //Creates and adds a Catch object without an image to listCatches.
        public void AddCatch(DateTime Time, String Species, String Location, String Bait, float WeatherTemperature, float WindSpeed, String WeatherConditions, float Length, float Weight, String WaterClarity, float Depth, float WaterTemperature, bool Method, String Note)
        {
            this.listCatches.Add(new Catch(Time, Properties.Resources.catches, Species, Location, Bait, WeatherTemperature, WindSpeed, WeatherConditions, Length, Weight, WaterClarity, Depth, WaterTemperature, Method, Note));
            Console.WriteLine("Catch succesfully added.");
        }

        //Removes a specific Catch object from listCatches.
        public void RemoveCatch(DateTime time, String species)
        {
            foreach (Catch c in listCatches)
            {
                if (c.Time == time && c.Species == species)
                {
                    if (this.listCatches.Remove(c))
                    {
                        Console.WriteLine(time + " has been removed successfully."); //CHANGE LATER
                        return;
                    }
                    else
                    {
                        Console.WriteLine("Unable to remove " + time); //CHANGE LATER
                        return;
                    }
                }
            }
            Console.WriteLine(time + "had not been added before."); //CHANGE LATER
        }

        //Saves catch information to appropriate file.
        public void SaveCatches()
        {
            try
            {
                Stream stream = File.Open(DATA_FILENAME, FileMode.Create);
                BinaryFormatter bFormatter = new BinaryFormatter();

                Console.WriteLine("Writing catches information.");
                bFormatter.Serialize(stream, this.listCatches);
                stream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("Unable to save catches information.");
            }
        }

        //Loads catch information from appropriate file.
        public void LoadCatches()
        {
            try
            {
                Stream stream = File.Open(DATA_FILENAME, FileMode.Open);
                BinaryFormatter bFormatter = new BinaryFormatter();

                Console.WriteLine("Reading catches information.");
                this.listCatches = (List<Catch>)bFormatter.Deserialize(stream);
                stream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("The file that contains catch information exists but there is a problem reading it.");
            }
        }
    }
}
