﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AFL
{
    public partial class TripAddForm : Form
    {
        public TripAddForm()
        {
            InitializeComponent();
        }

        public LocationInformation locationInformation = new LocationInformation();

        public String Name { get; set; }
        public String Location { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            Name = textBoxName.Text;
            Location = dropDownLocations.selectedValue.ToString();
            StartDate = dateTimePickerStartDate.Value;
            EndDate = dateTimePickerEndDate.Value;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void LoadInformation()
        {
            locationInformation.LoadLocations();
            foreach (Location l in locationInformation.listLocations)
            {
                dropDownLocations.AddItem(l.Name);
            }
        }
    }
}
