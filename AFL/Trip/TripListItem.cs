﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AFL
{
    public partial class TripListItem : UserControl
    {
        public TripListItem()
        {
            InitializeComponent();
        }

        public String Name { get; set; }
        public String Location { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public Button DeleteButton { get; set; }
        public Button EditButton { get; set; }

        public void SetButtons()
        {
            DeleteButton = buttonDelete;
            EditButton = buttonEdit;
        }

        public void ShowValues()
        {
            labelNameHere.Text = Name;
            labelLocationHere.Text = Location;
            labelStartDateHere.Text = StartDate.ToString("dd/MM/yyyy");
            labelEndDateHere.Text = EndDate.ToString("dd/MM/yyyy");
        }
    }
}
