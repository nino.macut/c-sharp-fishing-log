﻿namespace AFL
{
    partial class TripListItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelLocation = new System.Windows.Forms.Label();
            this.labelLocationHere = new System.Windows.Forms.Label();
            this.panelDecoration = new System.Windows.Forms.Panel();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.labelNameHere = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.labelStartDate = new System.Windows.Forms.Label();
            this.labelEndDate = new System.Windows.Forms.Label();
            this.labelStartDateHere = new System.Windows.Forms.Label();
            this.labelEndDateHere = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelLocation
            // 
            this.labelLocation.AutoSize = true;
            this.labelLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLocation.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelLocation.Location = new System.Drawing.Point(14, 35);
            this.labelLocation.Name = "labelLocation";
            this.labelLocation.Size = new System.Drawing.Size(73, 13);
            this.labelLocation.TabIndex = 40;
            this.labelLocation.Text = "LOCATION:";
            // 
            // labelLocationHere
            // 
            this.labelLocationHere.AutoSize = true;
            this.labelLocationHere.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLocationHere.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelLocationHere.Location = new System.Drawing.Point(125, 35);
            this.labelLocationHere.Name = "labelLocationHere";
            this.labelLocationHere.Size = new System.Drawing.Size(120, 13);
            this.labelLocationHere.TabIndex = 39;
            this.labelLocationHere.Text = "Location goes here!";
            // 
            // panelDecoration
            // 
            this.panelDecoration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.panelDecoration.Location = new System.Drawing.Point(375, 80);
            this.panelDecoration.Name = "panelDecoration";
            this.panelDecoration.Size = new System.Drawing.Size(150, 80);
            this.panelDecoration.TabIndex = 38;
            // 
            // buttonDelete
            // 
            this.buttonDelete.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.buttonDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.buttonDelete.FlatAppearance.BorderSize = 0;
            this.buttonDelete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(23)))), ((int)(((byte)(38)))));
            this.buttonDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDelete.ForeColor = System.Drawing.Color.Gainsboro;
            this.buttonDelete.Image = global::AFL.Properties.Resources.deleteIconPink;
            this.buttonDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonDelete.Location = new System.Drawing.Point(375, 40);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(150, 40);
            this.buttonDelete.TabIndex = 37;
            this.buttonDelete.Text = "     DELETE";
            this.buttonDelete.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonDelete.UseVisualStyleBackColor = false;
            // 
            // buttonEdit
            // 
            this.buttonEdit.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.buttonEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.buttonEdit.FlatAppearance.BorderSize = 0;
            this.buttonEdit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(23)))), ((int)(((byte)(38)))));
            this.buttonEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEdit.ForeColor = System.Drawing.Color.Gainsboro;
            this.buttonEdit.Image = global::AFL.Properties.Resources.editIconPink;
            this.buttonEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonEdit.Location = new System.Drawing.Point(375, 0);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(150, 40);
            this.buttonEdit.TabIndex = 36;
            this.buttonEdit.Text = "     EDIT";
            this.buttonEdit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonEdit.UseVisualStyleBackColor = false;
            // 
            // labelNameHere
            // 
            this.labelNameHere.AutoSize = true;
            this.labelNameHere.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNameHere.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelNameHere.Location = new System.Drawing.Point(125, 15);
            this.labelNameHere.Name = "labelNameHere";
            this.labelNameHere.Size = new System.Drawing.Size(103, 13);
            this.labelNameHere.TabIndex = 35;
            this.labelNameHere.Text = "Name goes here!";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelName.Location = new System.Drawing.Point(14, 15);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(46, 13);
            this.labelName.TabIndex = 34;
            this.labelName.Text = "NAME:";
            // 
            // labelStartDate
            // 
            this.labelStartDate.AutoSize = true;
            this.labelStartDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStartDate.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelStartDate.Location = new System.Drawing.Point(14, 55);
            this.labelStartDate.Name = "labelStartDate";
            this.labelStartDate.Size = new System.Drawing.Size(89, 13);
            this.labelStartDate.TabIndex = 41;
            this.labelStartDate.Text = "START DATE:";
            // 
            // labelEndDate
            // 
            this.labelEndDate.AutoSize = true;
            this.labelEndDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEndDate.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelEndDate.Location = new System.Drawing.Point(14, 75);
            this.labelEndDate.Name = "labelEndDate";
            this.labelEndDate.Size = new System.Drawing.Size(74, 13);
            this.labelEndDate.TabIndex = 42;
            this.labelEndDate.Text = "END DATE:";
            // 
            // labelStartDateHere
            // 
            this.labelStartDateHere.AutoSize = true;
            this.labelStartDateHere.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStartDateHere.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelStartDateHere.Location = new System.Drawing.Point(125, 55);
            this.labelStartDateHere.Name = "labelStartDateHere";
            this.labelStartDateHere.Size = new System.Drawing.Size(127, 13);
            this.labelStartDateHere.TabIndex = 43;
            this.labelStartDateHere.Text = "Start date goes here!";
            // 
            // labelEndDateHere
            // 
            this.labelEndDateHere.AutoSize = true;
            this.labelEndDateHere.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEndDateHere.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelEndDateHere.Location = new System.Drawing.Point(125, 75);
            this.labelEndDateHere.Name = "labelEndDateHere";
            this.labelEndDateHere.Size = new System.Drawing.Size(122, 13);
            this.labelEndDateHere.TabIndex = 44;
            this.labelEndDateHere.Text = "End date goes here!";
            // 
            // TripListItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(20)))), ((int)(((byte)(34)))));
            this.Controls.Add(this.labelEndDateHere);
            this.Controls.Add(this.labelStartDateHere);
            this.Controls.Add(this.labelEndDate);
            this.Controls.Add(this.labelStartDate);
            this.Controls.Add(this.labelLocation);
            this.Controls.Add(this.labelLocationHere);
            this.Controls.Add(this.panelDecoration);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonEdit);
            this.Controls.Add(this.labelNameHere);
            this.Controls.Add(this.labelName);
            this.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.Name = "TripListItem";
            this.Size = new System.Drawing.Size(525, 101);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelLocation;
        private System.Windows.Forms.Label labelLocationHere;
        private System.Windows.Forms.Panel panelDecoration;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Label labelNameHere;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelStartDate;
        private System.Windows.Forms.Label labelEndDate;
        private System.Windows.Forms.Label labelStartDateHere;
        private System.Windows.Forms.Label labelEndDateHere;
    }
}
