﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AFL
{
    public partial class TripEditForm : Form
    {
        public TripEditForm()
        {
            InitializeComponent();
        }

        private LocationInformation locationInformation = new LocationInformation();

        public String Name { get; set; }
        public String Location { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public void ShowValues()
        {
            textBoxName.Text = Name;

            //Set the location drop down menu to select the proper value.
            try{
                int selectedLocationIndex = 0;
                foreach (Location l in locationInformation.listLocations)
                {
                    if (l.Name == Location)
                    {
                        break;
                    }
                    selectedLocationIndex++;
                }
                dropDownLocations.selectedIndex = selectedLocationIndex;
            }
            catch(CustomExceptions.InvalidDropDownIndexException ex)
            {
                Console.WriteLine(ex.Message);
            }
            

            dateTimePickerStartDate.Value = StartDate;
            dateTimePickerEndDate.Value = EndDate;
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            Name = textBoxName.Text;
            Location = dropDownLocations.selectedValue.ToString(); ;
            StartDate = dateTimePickerStartDate.Value;
            EndDate = dateTimePickerEndDate.Value;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void LoadInformation()
        {
            locationInformation.LoadLocations();
            foreach (Location l in locationInformation.listLocations)
            {
                dropDownLocations.AddItem(l.Name);
            }
        }
    }
}
