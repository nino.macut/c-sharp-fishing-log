﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFL
{
    [Serializable]
    public class Trip
    {
        public Trip(String Name, String Location, DateTime StartDate, DateTime EndDate)
        {
            this.Name = Name;
            this.Location = Location;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
        }

        public String Name { get; set; }
        public String Location { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
