﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace AFL
{
    public class TripInformation : TripInformationInterface
    {
        public List<Trip> listTrips;

        //Trip information will be saved into this file.
        private const String DATA_FILENAME = "tripsInformation.dat";

        public TripInformation()
        {
            this.listTrips = new List<Trip>();
        }

        //Creates a trip object and adds it to listTrips.
        public void AddTrip(String name, String location, DateTime startDate, DateTime endDate)
        {
            foreach (Trip t in listTrips)
            {
                if (t.Name == name && t.Location == location)
                {
                    Console.WriteLine("You had already added " + name + " before.");
                    return;
                }
            }
            this.listTrips.Add(new Trip(name, location, startDate, endDate));
            Console.WriteLine("Trip succesfully added.");
        }

        //Removes a specific trip from listTrips
        public void RemoveTrip(String name, String location)
        {
            foreach (Trip t in listTrips)
            {
                if (t.Name == name && t.Location == location)
                {
                    if (this.listTrips.Remove(t))
                    {
                        Console.WriteLine(name + " has been removed successfully.");
                        return;
                    }
                    else
                    {
                        Console.WriteLine("Unable to remove " + name);
                        return;
                    }
                }
            }
            Console.WriteLine(name + "had not been added before.");
        }

        //Saves trip information into appropriate file
        public void SaveTrips()
        {
            try
            {
                Stream stream = File.Open(DATA_FILENAME, FileMode.Create);
                BinaryFormatter bFormatter = new BinaryFormatter();

                Console.WriteLine("Writing trip information.");
                bFormatter.Serialize(stream, this.listTrips);
                stream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("Unable to save trip information.");
            }
        }

        //Loads trip information from appropriate file.
        public void LoadTrips()
        {
            try
            {
                Stream stream = File.Open(DATA_FILENAME, FileMode.Open);
                BinaryFormatter bFormatter = new BinaryFormatter();

                Console.WriteLine("Reading trip information.");
                this.listTrips = (List<Trip>)bFormatter.Deserialize(stream);
                stream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("The file that contains trip information exists but there is a problem reading it.");
            }
        }
    }
}
