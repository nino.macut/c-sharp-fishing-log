﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFL
{
    interface TripInformationInterface
    {
        void AddTrip(String name, String location, DateTime startDate, DateTime endDate);
        void RemoveTrip(String name, String location);
        void SaveTrips();
        void LoadTrips();
    }
}
