﻿using AFL.Properties;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AFL
{
    public partial class FormMain : System.Windows.Forms.Form
    {
        public FormMain()
        {
            InitializeComponent();
            CustomizeDesign();
            #region Thread (Click + to expand)
            Thread tMain = Thread.CurrentThread;
            tMain.Name = "MainThread";
            Thread tChild = new Thread(() => ThreadProc(labelCurrentTime));
            //Thread tChild = new Thread(() => ThreadProc();
            tChild.Name = "ChildThread";
            tChild.Start();
            tChild.Join();
            #endregion
        }

        #region Variables (Click + to expand)
        private Form activeForm = null;
        private LoadHandler loadHandler = new LoadHandler();
        private FishermanInformation fishermanInformation = new FishermanInformation();
        FormChildMain fishermenChild = new FormChildMain();
        private BaitInformation baitInformation = new BaitInformation();
        FormChildMain baitsChild = new FormChildMain();
        private BaitCategoryInformation baitCategoriesInformation = new BaitCategoryInformation();
        FormChildMain baitCategoriesChild = new FormChildMain();
        private SpeciesInformation speciesInformation = new SpeciesInformation();
        FormChildMain speciesChild = new FormChildMain();
        private CatchInformation catchInformation = new CatchInformation();
        FormChildMain catchChild = new FormChildMain();
        private LocationInformation locationInformation = new LocationInformation();
        FormChildMain locationChild = new FormChildMain();
        private TripInformation tripInformation = new TripInformation();
        FormChildMain tripChild = new FormChildMain();
        #endregion

        #region Thread (Click + to expand)
        public delegate void delegateTime(string msg);

        public static async void ThreadProc(Label l)
        {
            for (int i = 0; i < 10; i++)
            {
                Task task = Task.Factory.StartNew(() =>
                {
                    while (true)
                    {
                        try
                        {
                            delegateTime delegateMessage = (String msg) =>
                            {
                                Console.WriteLine(msg);
                            };
                            String text = DateTime.Now.ToString("HH:mm:ss");
                            delegateMessage(text);
                            l.Invoke(new Action(() => l.Text = text));
                        }
                        catch(Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        Thread.Sleep(0);
                    }
                    
                });
                
                await task;
                
                Thread.Sleep(0);
            }
        }

        #endregion

        #region Class Functions (Click + to expand)
        private void CustomizeDesign()
        {
            panelFishermenSubmenu.Visible = false;
            panelLocationsSubmenu.Visible = false;
            panelTripsSubmenu.Visible = false;
            panelBaitsSubmenu.Visible = false;
            panelCatchesSubmenu.Visible = false;
            panelSpeciesSubmenu.Visible = false;
        }

        private void HideSubMenu()
        {
            if (panelFishermenSubmenu.Visible == true)
            {
                panelFishermenSubmenu.Visible = false;
            }
            if (panelLocationsSubmenu.Visible == true)
            {
                panelLocationsSubmenu.Visible = false;
            }
            if (panelTripsSubmenu.Visible == true)
            {
                panelTripsSubmenu.Visible = false;
            }
            if (panelBaitsSubmenu.Visible == true)
            {
                panelBaitsSubmenu.Visible = false;
            }
            if (panelCatchesSubmenu.Visible == true)
            {
                panelCatchesSubmenu.Visible = false;
            }
            if (panelSpeciesSubmenu.Visible == true)
            {
                panelSpeciesSubmenu.Visible = false;
            }
        }

        private void ShowSubMenu(Panel panelSubmenu)
        {
            if(panelSubmenu.Visible == false)
            {
                HideSubMenu();
                panelSubmenu.Visible = true;
            }
            else
            {
                panelSubmenu.Visible = false;
            }
        }

        private void OpenChildForm(Form childForm)
        {
            if(activeForm != null)
            {
                activeForm.Close();
            }
            if(childForm == null)
            {
                return;
            }
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            panelContainer.Controls.Add(childForm);
            panelContainer.Tag = childForm;
            childForm.BringToFront();
            try
            {
                childForm.Show();
            }
            catch(ObjectDisposedException e)
            {
                Console.WriteLine("Object has been disposed before.");
            }
        }
        #endregion

        #region Fishermen Menu (Click + to expand)
        public void ShowAddFishermanDialogBox()
        {
            FishermanAddForm dialog = new FishermanAddForm();

            //Show AddFishermanForm as a modal dialog and determine if DialogResult = Yes.
            //If dialog result is "Yes" then add new fisherman information and display it.
            if (dialog.ShowDialog(this) == DialogResult.Yes)
            {
                fishermanInformation.AddFisherman(dialog.ProfilePicture, dialog.FirstName, dialog.LastName, dialog.BirthDate);
                fishermanInformation.SaveFishermen();
                fishermanInformation.LoadFishermen();
                PopulateFishermen();
            }
            else
            {
                Console.WriteLine("Canceled");
            }
            dialog.Dispose();
        }

        public void ShowEditFishermanDialogBox(int index)
        {
            FishermanEditForm dialog = new FishermanEditForm();
            Fisherman f = fishermanInformation.listFishermen[index];
            dialog.ProfilePicture = f.ProfilePicture;
            dialog.FirstName = f.FirstName;
            dialog.LastName = f.LastName;
            dialog.BirthDate = f.BirthDate;
            dialog.ShowValues();

            //Show EditFishermanForm as a modal dialog and determine if DialogResult = Yes.
            //If dialog result is "Yes" then edit fisherman information and display it.
            if (dialog.ShowDialog(this) == DialogResult.Yes)
            {
                fishermanInformation.listFishermen[index].ProfilePicture = dialog.ProfilePicture;
                fishermanInformation.listFishermen[index].FirstName = dialog.FirstName;
                fishermanInformation.listFishermen[index].LastName = dialog.LastName;
                fishermanInformation.listFishermen[index].BirthDate = dialog.BirthDate;
                fishermanInformation.SaveFishermen();
                fishermanInformation.LoadFishermen();
                PopulateFishermen();
            }
            else
            {
                Console.WriteLine("Canceled");
            }
            dialog.Dispose();
        }

        private void PopulateFishermen()
        {
            //Clear the fishermen FlowLayoutPanel.
            if (fishermenChild.ChildFlowLayoutPanel.Controls.Count > 0)
            {
                fishermenChild.ChildFlowLayoutPanel.Controls.Clear();
            }

            int index = -1;

            //Loop through each fisherman.
            foreach (Fisherman f in fishermanInformation.listFishermen)
            {
                index++;

                FishermanListItem listItem = new FishermanListItem();
                listItem.ProfilePicture = f.ProfilePicture;
                listItem.FirstName = f.FirstName;
                listItem.LastName = f.LastName;
                listItem.BirthDate = f.BirthDate;

                //Add button events
                listItem.SetButtons();
                listItem.DeleteButton.Click += delegate
                {
                    fishermanInformation.RemoveFisherman(listItem.FirstName, listItem.LastName);
                    fishermanInformation.SaveFishermen();
                    fishermanInformation.LoadFishermen();
                    PopulateFishermen();
                };

                listItem.ShowValues();

                listItem.EditButton.Click += delegate
                {
                    ShowEditFishermanDialogBox(index);
                };

                //Add fisherman entity to FlowLayoutPanel.
                fishermenChild.ChildFlowLayoutPanel.Controls.Add(listItem);
                
            }
        }

        //FISHERMEN MENU BUTTONS
        private void buttonFishermen_Click(object sender, EventArgs e)
        {
            ShowSubMenu(panelFishermenSubmenu);
        }

        //Generates view fishermen form;
        private void buttonFishermenView_Click(object sender, EventArgs e)
        {
            fishermenChild = new FormChildMain();
            fishermenChild.AddButton.Text = "ADD FISHERMAN";
            fishermenChild.AddButton.Click += delegate
            {
                ShowAddFishermanDialogBox();
            };
            OpenChildForm(fishermenChild);
            fishermanInformation.LoadFishermen();
            PopulateFishermen();
            HideSubMenu();
        }

        private void buttonFishermenAdd_Click(object sender, EventArgs e)
        {
            ShowAddFishermanDialogBox();
        }
        #endregion

        #region Locations Menu (Click + to expand)

        public void ShowAddLocationDialogBox()
        {
            LocationAddForm dialog = new LocationAddForm();

            //Show LocationAddForm as a modal dialog and determine if DialogResult = Yes.
            //If dialog result is "Yes" then add new location information and display it.
            if (dialog.ShowDialog(this) == DialogResult.Yes)
            {
                locationInformation.AddLocation(dialog.Image, dialog.Name, dialog.Description);
                locationInformation.SaveLocations();
                locationInformation.LoadLocations();
                PopulateLocations();
            }
            else
            {
                Console.WriteLine("Canceled");
            }
            dialog.Dispose();
        }

        public void ShowEditLocationDialogBox(int index)
        {
            LocationEditForm dialog = new LocationEditForm();
            Location l = locationInformation.listLocations[index];
            dialog.Image = l.Image;
            dialog.Name = l.Name;
            dialog.Description = l.Description;
            dialog.ShowValues();

            //Show EditFishermanForm as a modal dialog and determine if DialogResult = Yes.
            //If dialog result is "Yes" then edit fisherman information and display it.
            if (dialog.ShowDialog(this) == DialogResult.Yes)
            {
                locationInformation.listLocations[index].Image = dialog.Image;
                locationInformation.listLocations[index].Name = dialog.Name;
                locationInformation.listLocations[index].Description = dialog.Description;
                locationInformation.SaveLocations();
                locationInformation.LoadLocations();
                PopulateLocations();
            }
            else
            {
                Console.WriteLine("Canceled");
            }
            dialog.Dispose();
        }

        private void PopulateLocations()
        {
            //Clear the location FlowLayoutPanel.
            if (locationChild.ChildFlowLayoutPanel.Controls.Count > 0)
            {
                locationChild.ChildFlowLayoutPanel.Controls.Clear();
            }

            int index = -1;

            //Loop through each fisherman.
            foreach (Location l in locationInformation.listLocations)
            {
                index++;

                LocationListItem listItem = new LocationListItem();
                listItem.Image = l.Image;
                listItem.Name = l.Name;
                listItem.Description = l.Description;

                //Add button events
                listItem.SetButtons();
                listItem.DeleteButton.Click += delegate
                {
                    locationInformation.RemoveLocation(listItem.Name);
                    locationInformation.SaveLocations();
                    locationInformation.LoadLocations();
                    PopulateLocations();
                };

                listItem.ShowValues();

                listItem.EditButton.Click += delegate
                {
                    ShowEditLocationDialogBox(index);
                };

                //Add to FlowLayoutPanel.
                locationChild.ChildFlowLayoutPanel.Controls.Add(listItem);

            }
        }

        //LOCATIONS MENU BUTTONS
        private void buttonLocations_Click(object sender, EventArgs e)
        {
            ShowSubMenu(panelLocationsSubmenu);
        }

        private void buttonLocationsView_Click(object sender, EventArgs e)
        {
            locationChild = new FormChildMain();
            locationChild.AddButton.Text = "ADD LOCATION";
            locationChild.AddButton.Click += delegate
            {
                ShowAddLocationDialogBox();
            };
            OpenChildForm(locationChild);
            locationInformation.LoadLocations();
            PopulateLocations();
            HideSubMenu();
        }

        private void buttonLocationsAdd_Click(object sender, EventArgs e)
        {
            ShowAddLocationDialogBox();
            HideSubMenu();
        }
        #endregion

        #region Trips Menu (Click + to expand)

        public void ShowAddTripDialogBox()
        {
            TripAddForm dialog = new TripAddForm();
            dialog.LoadInformation();

            //Show TripAddForm as a modal dialog and determine if DialogResult = Yes.
            //If dialog result is "Yes" then add new trip information and display it.
            if (dialog.ShowDialog(this) == DialogResult.Yes)
            {
                tripInformation.AddTrip(dialog.Name, dialog.Location, dialog.StartDate, dialog.EndDate);
                tripInformation.SaveTrips();
                tripInformation.LoadTrips();
                PopulateTrips();
            }
            else
            {
                Console.WriteLine("Canceled");
            }
            dialog.Dispose();
        }

        public void ShowEditTripDialogBox(int index)
        {
            TripEditForm dialog = new TripEditForm();
            dialog.LoadInformation();
            Trip t = tripInformation.listTrips[index];
            dialog.Name = t.Name;
            dialog.Location = t.Location;
            dialog.StartDate = t.StartDate;
            dialog.EndDate = t.EndDate;
            dialog.ShowValues();

            //Show TripEditForm as a modal dialog and determine if DialogResult = Yes.
            //If dialog result is "Yes" then edit trip information and display it.
            if (dialog.ShowDialog(this) == DialogResult.Yes)
            {
                tripInformation.listTrips[index].Name = dialog.Name;
                tripInformation.listTrips[index].Location = dialog.Location;
                tripInformation.listTrips[index].StartDate = dialog.StartDate;
                tripInformation.listTrips[index].EndDate = dialog.EndDate;
                tripInformation.SaveTrips();
                tripInformation.LoadTrips();
                PopulateTrips();
            }
            else
            {
                Console.WriteLine("Canceled");
            }
            dialog.Dispose();
        }

        private void PopulateTrips()
        {
            //Clear the trip FlowLayoutPanel.
            if (tripChild.ChildFlowLayoutPanel.Controls.Count > 0)
            {
                tripChild.ChildFlowLayoutPanel.Controls.Clear();
            }

            int index = -1;

            //Loop through each trip.
            foreach (Trip t in tripInformation.listTrips)
            {
                index++;

                TripListItem listItem = new TripListItem();
                listItem.Name = t.Name;
                listItem.Location = t.Location;
                listItem.StartDate = t.StartDate;
                listItem.EndDate = t.EndDate;

                //Add button events
                listItem.SetButtons();
                listItem.DeleteButton.Click += delegate
                {
                    tripInformation.RemoveTrip(listItem.Name,listItem.Location);
                    tripInformation.SaveTrips();
                    tripInformation.LoadTrips();
                    PopulateTrips();
                };

                listItem.ShowValues();

                listItem.EditButton.Click += delegate
                {
                    ShowEditTripDialogBox(index);
                };

                //Add to FlowLayoutPanel.
                tripChild.ChildFlowLayoutPanel.Controls.Add(listItem);

            }
        }

        //TRIPS MENU BUTTONS
        private void buttonTrips_Click(object sender, EventArgs e)
        {
            ShowSubMenu(panelTripsSubmenu);
        }

        private void buttonTripsView_Click(object sender, EventArgs e)
        {
            tripChild = new FormChildMain();
            tripChild.AddButton.Text = "PLAN TRIP";
            tripChild.AddButton.Click += delegate
            {
                ShowAddTripDialogBox();
            };
            OpenChildForm(tripChild);
            tripInformation.LoadTrips();
            PopulateTrips();
            HideSubMenu();
        }

        private void buttonTripsPlan_Click(object sender, EventArgs e)
        {
            ShowAddTripDialogBox();
            HideSubMenu();
        }
        #endregion

        #region Baits Menu (Click + to expand)
        //BAITS MENU FUNCTIONS
        public void ShowAddBaitDialogBox()
        {
            BaitAddForm dialog = new BaitAddForm();

            //Show BaitAddForm as a modal dialog and determine if DialogResult = Yes.
            //If dialog result is "Yes" then add new fisherman information and display it.
            if (dialog.ShowDialog(this) == DialogResult.Yes)
            {
                baitInformation.AddBait(dialog.Image, dialog.Category, dialog.Type, dialog.Name, dialog.Color, dialog.Brand, dialog.Model, dialog.Description, dialog.HasRecipe, dialog.Recipe);
                baitInformation.SaveBait();
                baitInformation.LoadBait();
                baitCategoriesInformation.AddBaitCategory(dialog.Category);
                baitCategoriesInformation.SaveBaitCategories();
                baitCategoriesInformation.LoadBaitCategories();
                PopulateBaitCategories();
                PopulateBaits();
            }
            else
            {
                Console.WriteLine("Canceled");
            }
            dialog.Dispose();
        }

        public void ShowEditBaitDialogBox(int index)
        {
            BaitEditForm dialog = new BaitEditForm();
            Bait b = baitInformation.listBait[index];
            dialog.Image = b.Image;
            dialog.Category = b.Category;
            dialog.Type = b.Type;
            dialog.Name = b.Name;
            dialog.Color = b.Color;
            dialog.Brand = b.Brand;
            dialog.Model = b.Model;
            dialog.Description = b.Description;
            dialog.HasRecipe = b.HasRecipe;
            dialog.Recipe = b.Recipe;
            dialog.ShowValues();

            //Show BaitEditForm as a modal dialog and determine if DialogResult = Yes.
            //If dialog result is "Yes" then edit bait information and display it.
            if (dialog.ShowDialog(this) == DialogResult.Yes)
            {
                baitInformation.listBait[index].Image = dialog.Image;
                baitInformation.listBait[index].Category = dialog.Category;
                baitInformation.listBait[index].Type = dialog.Type;
                baitInformation.listBait[index].Name = dialog.Name;
                baitInformation.listBait[index].Color = dialog.Color;
                baitInformation.listBait[index].Brand = dialog.Brand;
                baitInformation.listBait[index].Model = dialog.Model;
                baitInformation.listBait[index].Description = dialog.Description;
                baitInformation.listBait[index].HasRecipe = dialog.HasRecipe;
                baitInformation.listBait[index].Recipe = dialog.Recipe;
                baitInformation.SaveBait();
                baitInformation.LoadBait();
                PopulateBaits();

                baitCategoriesInformation.AddBaitCategory(dialog.Category);
                baitCategoriesInformation.SaveBaitCategories();
                baitCategoriesInformation.LoadBaitCategories();
                PopulateBaitCategories();
            }
            else
            {
                Console.WriteLine("Canceled");
            }
            dialog.Dispose();
        }

        public void ShowAddBaitCategoryDialogBox()
        {
            BaitCategoryAddForm dialog = new BaitCategoryAddForm();

            //Show BaitCategoryAddForm as a modal dialog and determine if DialogResult = Yes.
            //If dialog result is "Yes" then add new bait category information and display it.
            if (dialog.ShowDialog(this) == DialogResult.Yes)
            {
                baitCategoriesInformation.AddBaitCategory(dialog.Name);
                baitCategoriesInformation.SaveBaitCategories();
                baitCategoriesInformation.LoadBaitCategories();
                PopulateBaitCategories();
            }
            else
            {
                Console.WriteLine("Canceled");
            }
            dialog.Dispose();
        }

        private void PopulateBaits()
        {
            //Clear the baits FlowLayoutPanel.
            if (baitsChild.ChildFlowLayoutPanel.Controls.Count > 0)
            {
                baitsChild.ChildFlowLayoutPanel.Controls.Clear();
            }

            int index = -1;

            //Loop through each bait.
            foreach (Bait b in baitInformation.listBait)
            {
                index++;
                
                if(b.HasRecipe == false)
                {
                    BaitListItemWithoutRecipe listItem = new BaitListItemWithoutRecipe();
                    listItem.Image = b.Image;
                    listItem.Category = b.Category;
                    listItem.Name = b.Name;
                    listItem.Type = b.Type;
                    listItem.Color = b.Color;
                    listItem.Brand = b.Brand;
                    listItem.Model = b.Model;
                    listItem.Description = b.Description;
                    listItem.HasRecipe = b.HasRecipe;
                    listItem.Recipe = b.Recipe;

                    //Add button events
                    listItem.SetButtons();
                    listItem.DeleteButton.Click += delegate
                    {
                        baitInformation.RemoveBait(listItem.Category, listItem.Name);
                        baitInformation.SaveBait();
                        baitInformation.LoadBait();
                        PopulateBaits();
                    };

                    listItem.ShowValues();

                    listItem.EditButton.Click += delegate
                    {
                        ShowEditBaitDialogBox(index);
                    };

                    //Add to FlowLayoutPanel.
                    baitsChild.ChildFlowLayoutPanel.Controls.Add(listItem);
                }
                else if (b.HasRecipe == true)
                {
                    BaitListItemWithRecipe listItem = new BaitListItemWithRecipe();
                    listItem.Image = b.Image;
                    listItem.Category = b.Category;
                    listItem.Name = b.Name;
                    listItem.Color = b.Color;
                    listItem.Brand = b.Brand;
                    listItem.Model = b.Model;
                    listItem.Description = b.Description;
                    listItem.HasRecipe = b.HasRecipe;
                    listItem.Recipe = b.Recipe;

                    //Add button events
                    listItem.SetButtons();
                    listItem.DeleteButton.Click += delegate
                    {
                        baitInformation.RemoveBait(listItem.Category, listItem.Name);
                        baitInformation.SaveBait();
                        baitInformation.LoadBait();
                        PopulateBaits();
                    };

                    listItem.ShowValues();

                    listItem.EditButton.Click += delegate
                    {
                        ShowEditBaitDialogBox(index);
                    };

                    //Add to FlowLayoutPanel.
                    baitsChild.ChildFlowLayoutPanel.Controls.Add(listItem);
                }
            }
        }

        public void PopulateBaitCategories()
        {
            //Clear the bait categories FlowLayoutPanel.
            if (baitCategoriesChild.ChildFlowLayoutPanel.Controls.Count > 0)
            {
                baitCategoriesChild.ChildFlowLayoutPanel.Controls.Clear();
            }

            int index = -1;

            //Loop through each bait category.
            foreach (BaitCategory b in baitCategoriesInformation.listBaitCategories)
            {
                index++;

                BaitCategoryListItem listItem = new BaitCategoryListItem();
                listItem.Name = b.Name;

                //Add button events
                listItem.SetButtons();
                listItem.DeleteButton.Click += delegate
                {
                    baitCategoriesInformation.RemoveBaitCategory(listItem.Name);
                    baitCategoriesInformation.SaveBaitCategories();
                    baitCategoriesInformation.LoadBaitCategories();
                    PopulateBaitCategories();
                };

                listItem.ShowValues();

                //Add to FlowLayoutPanel.
                baitCategoriesChild.ChildFlowLayoutPanel.Controls.Add(listItem);
            }
        }
        //BAITS MENU BUTTONS
        private void buttonBaits_Click(object sender, EventArgs e)
        {
            ShowSubMenu(panelBaitsSubmenu);
        }

        private void buttonBaitsViewCategories_Click(object sender, EventArgs e)
        {
            baitCategoriesChild = new FormChildMain();
            baitCategoriesChild.AddButton.Text = "ADD BAIT CATEGORY";
            baitCategoriesChild.AddButton.Click += delegate
            {
                ShowAddBaitCategoryDialogBox();
            };
            OpenChildForm(baitCategoriesChild);
            baitCategoriesInformation.LoadBaitCategories();
            PopulateBaitCategories();
            HideSubMenu();
        }

        private void buttonBaitsAddCategory_Click(object sender, EventArgs e)
        {
            ShowAddBaitCategoryDialogBox();
            HideSubMenu();
        }

        private void buttonBaitsView_Click(object sender, EventArgs e)
        {
            baitsChild = new FormChildMain();
            baitsChild.AddButton.Text = "ADD BAIT";
            baitsChild.AddButton.Click += delegate
            {
                ShowAddBaitDialogBox();
            };
            OpenChildForm(baitsChild);
            baitInformation.LoadBait();
            PopulateBaits();
            HideSubMenu();
        }
        private void buttonBaitsAdd_Click(object sender, EventArgs e)
        {
            ShowAddBaitDialogBox();
            HideSubMenu();
        }
        #endregion

        #region Catches Menu (Click + to expand)

        public void ShowAddCatchDialogBox()
        {
            CatchAddForm dialog = new CatchAddForm();
            dialog.LoadInformation();

            //Show CatchAddForm as a modal dialog and determine if DialogResult = Yes.
            //If dialog result is "Yes" then add new catch information and display it.
            if (dialog.ShowDialog(this) == DialogResult.Yes)
            {
                catchInformation.AddCatch(dialog.Time, dialog.Image, dialog.Species, dialog.Location, dialog.Bait, dialog.WeatherTemperature, dialog.WindSpeed, dialog.WeatherConditions, dialog.Length, dialog.Weight, dialog.WaterClarity, dialog.Depth, dialog.WaterTemperature, dialog.Method, dialog.Note);
                catchInformation.SaveCatches();
                catchInformation.LoadCatches();
                PopulateCatches();
            }
            else
            {
                Console.WriteLine("Canceled");
            }
            dialog.Dispose();
        }

        public void ShowEditCatchDialogBox(int index)
        {
            CatchEditForm dialog = new CatchEditForm();
            dialog.LoadInformation();
            Catch c = catchInformation.listCatches[index];
            dialog.Time = c.Time;
            dialog.Image = c.Image;
            dialog.Species = c.Species;
            dialog.Location = c.Location;
            dialog.Bait = c.Bait;
            dialog.WeatherTemperature = c.WeatherTemperature;
            dialog.WindSpeed = c.WindSpeed;
            dialog.WeatherConditions = c.WeatherConditions;
            dialog.Length = c.Length;
            dialog.Weight = c.Weight;
            dialog.WaterClarity = c.WaterClarity;
            dialog.Depth = c.Depth;
            dialog.WaterTemperature = c.WaterTemperature;
            dialog.Method = c.Method;
            dialog.Note = c.Note;
            dialog.ShowValues();

            //Show CatchEditForm as a modal dialog and determine if DialogResult = Yes.
            //If dialog result is "Yes" then edit catch information and display it.
            if (dialog.ShowDialog(this) == DialogResult.Yes)
            {
                catchInformation.listCatches[index].Time = dialog.Time;
                catchInformation.listCatches[index].Image = dialog.Image;
                catchInformation.listCatches[index].Species = dialog.Species;
                catchInformation.listCatches[index].Location = dialog.Location;
                catchInformation.listCatches[index].Bait = dialog.Bait;
                catchInformation.listCatches[index].WeatherTemperature = dialog.WeatherTemperature;
                catchInformation.listCatches[index].WindSpeed = dialog.WindSpeed;
                catchInformation.listCatches[index].WeatherConditions = dialog.WeatherConditions;
                catchInformation.listCatches[index].Length = dialog.Length;
                catchInformation.listCatches[index].Weight = dialog.Weight;
                catchInformation.listCatches[index].WaterClarity = dialog.WaterClarity;
                catchInformation.listCatches[index].Depth = dialog.Depth;
                catchInformation.listCatches[index].WaterTemperature = dialog.WaterTemperature;
                catchInformation.listCatches[index].Method = dialog.Method;
                catchInformation.listCatches[index].Note = dialog.Note;
                catchInformation.SaveCatches();
                catchInformation.LoadCatches();
                PopulateCatches();
            }
            else
            {
                Console.WriteLine("Canceled");
            }
            dialog.Dispose();
        }

        public void ShowViewCatchDialogBox(int index)
        {
            CatchViewForm dialog = new CatchViewForm();
            Catch c = catchInformation.listCatches[index];
            dialog.Time = c.Time;
            dialog.Image = c.Image;
            dialog.Species = c.Species;
            dialog.Location = c.Location;
            dialog.Bait = c.Bait;
            dialog.WeatherTemperature = c.WeatherTemperature;
            dialog.WindSpeed = c.WindSpeed;
            dialog.WeatherConditions = c.WeatherConditions;
            dialog.Length = c.Length;
            dialog.Weight = c.Weight;
            dialog.WaterClarity = c.WaterClarity;
            dialog.Depth = c.Depth;
            dialog.WaterTemperature = c.WaterTemperature;
            dialog.Method = c.Method;
            dialog.Note = c.Note;
            dialog.ShowValues();
            if (dialog.ShowDialog(this) == DialogResult.Yes)
            {
                Console.WriteLine("Successful");
            }
            else
            {
                Console.WriteLine("Canceled");
            }
            dialog.Dispose();
        }

        private void PopulateCatches()
        {
            //Clear the catches FlowLayoutPanel.
            if (catchChild.ChildFlowLayoutPanel.Controls.Count > 0)
            {
                catchChild.ChildFlowLayoutPanel.Controls.Clear();
            }

            int index = -1;

            //Loop through each fisherman.
            foreach (Catch c in catchInformation.listCatches)
            {
                index++;

                CatchListItem listItem = new CatchListItem();
                listItem.Time = c.Time;
                listItem.Image = c.Image;
                listItem.Species = c.Species;
                listItem.Location = c.Location;
                listItem.Bait = c.Bait;

                //Add button events
                listItem.SetButtons();
                listItem.DeleteButton.Click += delegate
                {
                    catchInformation.RemoveCatch(listItem.Time, listItem.Species);
                    catchInformation.SaveCatches();
                    catchInformation.LoadCatches();
                    PopulateCatches();
                };

                listItem.ShowValues();

                listItem.EditButton.Click += delegate
                {
                    ShowEditCatchDialogBox(index);
                };

                listItem.ViewButton.Click += delegate
                {
                    ShowViewCatchDialogBox(index);
                };

                //Add to FlowLayoutPanel.
                catchChild.ChildFlowLayoutPanel.Controls.Add(listItem);

            }
        }

        //CATCHES MENU BUTTONS
        private void buttonCatches_Click(object sender, EventArgs e)
        {
            ShowSubMenu(panelCatchesSubmenu);
        }

        private void buttonCatchesView_Click(object sender, EventArgs e)
        {
            catchChild = new FormChildMain();
            catchChild.AddButton.Text = "ADD CATCH";
            catchChild.AddButton.Click += delegate
            {
                ShowAddCatchDialogBox();
            };
            OpenChildForm(catchChild);
            catchInformation.LoadCatches();
            PopulateCatches();
            HideSubMenu();
        }

        private void buttonCatchesAdd_Click(object sender, EventArgs e)
        {
            ShowAddCatchDialogBox();
            HideSubMenu();
        }
        #endregion

        #region Window Control Buttons (Click + to expand)
        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonToggle_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                buttonToggle.Image = Resources.maximizeIconPink;
                this.WindowState = FormWindowState.Normal;
            }
            if (this.WindowState == FormWindowState.Normal)
            {
                buttonToggle.Image = Resources.minimizeIconPink;
                this.WindowState = FormWindowState.Maximized;
            }
        }

        private void buttonMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        #endregion

        #region Species Menu (Click + to expand)

        public void ShowAddSpeciesDialogBox()
        {
            SpeciesAddForm dialog = new SpeciesAddForm();

            //Show SpeciesAddForm as a modal dialog and determine if DialogResult = Yes.
            //If dialog result is "Yes" then add new species information and display it.
            if (dialog.ShowDialog(this) == DialogResult.Yes)
            {
                speciesInformation.AddSpecies(dialog.Image, dialog.Name, dialog.IsPredator, dialog.Description);
                speciesInformation.SaveSpecies();
                speciesInformation.LoadSpecies();
                PopulateSpecies();
            }
            else
            {
                Console.WriteLine("Canceled");
            }
            dialog.Dispose();
        }

        public void ShowAddSpeciesDialogBox(String SpeciesName)
        {
            SpeciesAddForm dialog = new SpeciesAddForm();
            dialog.Name = SpeciesName;
            dialog.ShowValues();

            //Show SpeciesAddForm as a modal dialog and determine if DialogResult = Yes.
            //If dialog result is "Yes" then add new species information and display it.
            if (dialog.ShowDialog(this) == DialogResult.Yes)
            {
                speciesInformation.AddSpecies(dialog.Image, dialog.Name, dialog.IsPredator, dialog.Description);
                speciesInformation.SaveSpecies();
                speciesInformation.LoadSpecies();
                PopulateSpecies();
            }
            else
            {
                Console.WriteLine("Canceled");
            }
            dialog.Dispose();
        }
        public void ShowEditSpeciesDialogBox(int index)
        {
            SpeciesEditForm dialog = new SpeciesEditForm();
            Species s = speciesInformation.listSpecies[index];
            dialog.Image = s.Image;
            dialog.Name = s.Name;
            dialog.IsPredator = s.IsPredator;
            dialog.Description = s.Description;
            dialog.ShowValues();

            //Show SpeciesEditForm as a modal dialog and determine if DialogResult = Yes.
            //If dialog result is "Yes" then edit species information and display it.
            if (dialog.ShowDialog(this) == DialogResult.Yes)
            {
                speciesInformation.listSpecies[index].Image = dialog.Image;
                speciesInformation.listSpecies[index].Name = dialog.Name;
                speciesInformation.listSpecies[index].IsPredator = dialog.IsPredator;
                speciesInformation.listSpecies[index].Description = dialog.Description;
                speciesInformation.SaveSpecies();
                speciesInformation.LoadSpecies();
                PopulateSpecies();
            }
            else
            {
                Console.WriteLine("Canceled");
            }
            dialog.Dispose();
        }

        private void PopulateSpecies()
        {
            //Clear the species FlowLayoutPanel.
            if (speciesChild.ChildFlowLayoutPanel.Controls.Count > 0)
            {
                speciesChild.ChildFlowLayoutPanel.Controls.Clear();
            }

            int index = -1;

            //Loop through each species.
            foreach (Species s in speciesInformation.listSpecies)
            {
                index++;

                SpeciesListItem listItem = new SpeciesListItem();
                listItem.Image = s.Image;
                listItem.Name = s.Name;
                listItem.IsPredator = s.IsPredator;
                listItem.Description = s.Description;

                //Add button events
                listItem.SetButtons();
                listItem.DeleteButton.Click += delegate
                {
                    speciesInformation.RemoveSpecies(listItem.Name);
                    speciesInformation.SaveSpecies();
                    speciesInformation.LoadSpecies();
                    PopulateSpecies();
                };

                listItem.ShowValues();

                listItem.EditButton.Click += delegate
                {
                    ShowEditSpeciesDialogBox(index);
                };

                //Add to FlowLayoutPanel.
                speciesChild.ChildFlowLayoutPanel.Controls.Add(listItem);
            }
        }

        private void buttonSpecies_Click(object sender, EventArgs e)
        {
            ShowSubMenu(panelSpeciesSubmenu);
        }

        private void buttonViewSpecies_Click(object sender, EventArgs e)
        {
            speciesChild = new FormChildMain();
            speciesChild.AddButton.Text = "ADD SPECIES";
            speciesChild.AddButton.Click += delegate
            {
                ShowAddSpeciesDialogBox();
            };
            OpenChildForm(speciesChild);
            speciesInformation.LoadSpecies();
            PopulateSpecies();
            HideSubMenu();
        }

        private void buttonAddSpecies_Click(object sender, EventArgs e)
        {
            ShowAddSpeciesDialogBox();
        }
        #endregion
    }
}