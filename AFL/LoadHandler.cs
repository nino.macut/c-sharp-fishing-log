﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace AFL
{
    public class LoadHandler : LoadFileSystem
    {
        private const String DATA_FILENAME = "loadTimes.txt";
        public List<DateTime> listTimes = new List<DateTime>();

        public LoadHandler()
        {
            LoadLoadTimes();
            this.baitInformation.LoadBait();
            this.baitCategoryInformation.LoadBaitCategories();
            this.catchInformation.LoadCatches();
            this.fishermanInformation.LoadFishermen();
            this.locationInformation.LoadLocations();
            this.speciesInformation.LoadSpecies();
            listTimes.Add(DateTime.Now);
            SaveLoadTimes();
        }

        //Loads all needed information.
        public void LoadFiles()
        {
            baitInformation.LoadBait();
            baitCategoryInformation.LoadBaitCategories();
            catchInformation.LoadCatches();
            fishermanInformation.LoadFishermen();
            locationInformation.LoadLocations();
            speciesInformation.LoadSpecies();
            listTimes.Add(DateTime.Now);
            SaveLoadTimes();
        }

        public void SaveLoadTimes()
        {
            try
            {
                Stream stream = File.Open(DATA_FILENAME, FileMode.Create);
                BinaryFormatter bFormatter = new BinaryFormatter();

                Console.WriteLine("Writing load information.");
                bFormatter.Serialize(stream, this.listTimes);
                stream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("Unable to save load information.");
            }
        }

        public void LoadLoadTimes()
        {
            try
            {
                Stream stream = File.Open(DATA_FILENAME, FileMode.Open);
                BinaryFormatter bFormatter = new BinaryFormatter();

                Console.WriteLine("Reading load information.");
                this.listTimes = (List<DateTime>)bFormatter.Deserialize(stream);
                stream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("The file that contains load information exists but there is a problem reading it.");
            }
        }
    }
}
