﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AFL
{
    public partial class CatchEditForm : Form
    {
        public CatchEditForm()
        {
            InitializeComponent();
        }

        public Image Image { get; set; }
        public DateTime Time { get; set; }
        public String Species { get; set; }
        public String Location { get; set; }
        public String Bait { get; set; }
        public float WeatherTemperature { get; set; }
        public float WindSpeed { get; set; }
        public String WeatherConditions { get; set; }
        public float Length { get; set; }
        public float Weight { get; set; }
        public String WaterClarity { get; set; }
        public float Depth { get; set; }
        public float WaterTemperature { get; set; }
        public bool Method { get; set; }
        public String Note { get; set; }

        public SpeciesInformation speciesInformation = new SpeciesInformation();
        public BaitInformation baitInformation = new BaitInformation();

        public void ShowValues()
        {
            pictureBoxImage.Image = Image;
            dateTimePickerTime.Value = Time;

            int selectedSpeciesIndex = 0;
            foreach (Species s in speciesInformation.listSpecies)
            {
                if (s.Name == Species)
                {
                    break;
                }
                selectedSpeciesIndex++;
            }
            dropDownSpecies.selectedIndex = selectedSpeciesIndex;

            textBoxLocation.Text = Location;

            int selectedBaitIndex = 0;
            foreach (Bait b in baitInformation.listBait)
            {
                if (b.Name == Bait)
                {
                    break;
                }
                selectedSpeciesIndex++;
            }
            dropDownBait.selectedIndex = selectedBaitIndex;

            textBoxWeatherTemperature.Text = WeatherTemperature.ToString();
            textBoxWindSpeed.Text = WindSpeed.ToString();
            richTextBoxWeatherConditions.Text = WeatherConditions;
            textBoxLength.Text = Length.ToString();
            textBoxWeight.Text = Weight.ToString();
            textBoxWaterClarity.Text = WaterClarity;
            textBoxDepth.Text = Depth.ToString();
            textBoxWaterTemperature.Text = WaterTemperature.ToString();
            checkBoxMethod.Checked = Method;
            richTextBoxNote.Text = Note;
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            Image = pictureBoxImage.Image;
            Time = dateTimePickerTime.Value;
            Species = dropDownSpecies.selectedValue.ToString();
            Location = textBoxLocation.Text;
            Bait = dropDownBait.selectedValue.ToString();
            WeatherTemperature = (float)Convert.ToDouble(textBoxWeatherTemperature.Text);
            WindSpeed = (float)Convert.ToDouble(textBoxWindSpeed.Text);
            WeatherConditions = richTextBoxWeatherConditions.Text;
            Length = (float)Convert.ToDouble(textBoxLength.Text);
            Weight = (float)Convert.ToDouble(textBoxWeight.Text);
            WaterClarity = textBoxWaterClarity.Text;
            Depth = (float)Convert.ToDouble(textBoxDepth.Text);
            WaterTemperature = (float)Convert.ToDouble(textBoxWaterTemperature.Text);
            Method = checkBoxMethod.Checked;
            Note = richTextBoxNote.Text;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonUploadImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog chooseImage = new OpenFileDialog();
            chooseImage.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (chooseImage.ShowDialog() == DialogResult.OK)
            {
                pictureBoxImage.Image = new Bitmap(chooseImage.FileName);
            }
        }

        public void LoadInformation()
        {
            speciesInformation.LoadSpecies();
            baitInformation.LoadBait();
            foreach (Species s in speciesInformation.listSpecies)
            {
                dropDownSpecies.AddItem(s.Name);
            }
            foreach (Bait b in baitInformation.listBait)
            {
                dropDownBait.AddItem(b.Name);
            }
        }
    }
}
