﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace AFL
{
    public partial class SpeciesAddForm : Form
    {
        public SpeciesAddForm()
        {
            InitializeComponent();
        }

        private void buttonUploadImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog chooseImage = new OpenFileDialog();
            chooseImage.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (chooseImage.ShowDialog() == DialogResult.OK)
            {
                pictureBoxImage.Image = new Bitmap(chooseImage.FileName);
            }
        }

        public Image Image { get; set; }
        public String Name { get; set; }
        public bool IsPredator { get; set; }
        public String Description { get; set; }

        public void ShowValues()
        {
            textBoxName.Text = Name;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            Image = pictureBoxImage.Image;
            Name = textBoxName.Text;
            IsPredator = checkBoxIsPredator.Checked;
            Description = richTextBoxDescription.Text;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
