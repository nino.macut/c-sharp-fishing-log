﻿using System;
using System.Drawing;

namespace AFL
{
    [Serializable]
    public class Species
    {
        public Species(Image Image, String Name, bool IsPredator, String Description)
        {
            this.Image = Image;
            this.Name = Name;
            this.IsPredator = IsPredator;
            this.Description = Description;
        }
        public Image Image { get; set; }
        public String Name { get; set; }
        public bool IsPredator { get; set; }
        public String Description { get; set; }

        public override string ToString()
        {
            return ("SPECIES:\nName: " + Name + "\nPredator: " + IsPredator.ToString());
        }
    }
}
