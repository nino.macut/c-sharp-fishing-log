﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFL
{
    interface SpeciesInformationInterface
    {
        void AddSpecies(Image image, String name, bool isPredator, String description);
        void AddSpecies(String name, bool isPredator, String description);
        void RemoveSpecies(String name);
        void SaveSpecies();
        void LoadSpecies();
    }
}
