﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace AFL
{
    public partial class SpeciesEditForm : Form
    {
        public SpeciesEditForm()
        {
            InitializeComponent();
            checkBoxIsPredator.Enabled = true;
        }

        public Image Image { get; set; }
        public String Name { get; set; }
        public bool IsPredator { get; set; }
        public String Description { get; set; }

        public void ShowValues()
        {
            pictureBoxImage.Image = Image;
            textBoxName.Text = Name;
            checkBoxIsPredator.Checked = IsPredator;
            richTextBoxDescription.Text = Description;
        }

        private void buttonUploadImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog chooseImage = new OpenFileDialog();
            chooseImage.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (chooseImage.ShowDialog() == DialogResult.OK)
            {
                pictureBoxImage.Image = new Bitmap(chooseImage.FileName);
            }
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            Image = pictureBoxImage.Image;
            Name = textBoxName.Text;
            IsPredator = checkBoxIsPredator.Enabled;
            Description = richTextBoxDescription.Text;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
