﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace AFL
{
    public class SpeciesInformation : SpeciesInformationInterface
    {
        public List<Species> listSpecies;

        //Species information will be saved into this file.
        private const String DATA_FILENAME = "speciesInformation.dat";

        public SpeciesInformation()
        {
            this.listSpecies = new List<Species>();
        }

        //Adds a new Species object to listSpecies.
        public void AddSpecies(Image image, String name, bool isPredator, String description)
        {
            foreach (Species s in listSpecies)
            {
                if (s.Name == name)
                {
                    Console.WriteLine("You had already added " + name + " before.");
                    return;
                }
            }
            this.listSpecies.Add(new Species(image, name, isPredator, description));
            Console.WriteLine("Species succesfully added.");
        }

        //Adds a new Species object without an image to listSpecies.
        public void AddSpecies(String name, bool isPredator, String description)
        {
            foreach (Species s in listSpecies)
            {
                if (s.Name == name)
                {
                    Console.WriteLine("You had already added " + name + " before.");
                    return;
                }
            }
            this.listSpecies.Add(new Species(Properties.Resources.species, name, isPredator, description));
            Console.WriteLine("Species succesfully added.");
        }

        //Removes a specific species from listSpecies.
        public void RemoveSpecies(String name)
        {
            foreach (Species s in listSpecies)
            {
                if (s.Name == name)
                {
                    if (this.listSpecies.Remove(s))
                    {
                        Console.WriteLine(name + " has been removed successfully.");
                        return;
                    }
                    else
                    {
                        Console.WriteLine("Unable to remove " + name);
                        return;
                    }
                }
            }
            Console.WriteLine(name + "had not been added before.");
        }

        //Saves species information into appropriate file.
        public void SaveSpecies()
        {
            try
            {
                Stream stream = File.Open(DATA_FILENAME, FileMode.Create);
                BinaryFormatter bFormatter = new BinaryFormatter();

                Console.WriteLine("Writing species information.");
                bFormatter.Serialize(stream, this.listSpecies);
                stream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("Unable to save species information.");
            }
        }

        //Loads species information from appropriate file.
        public void LoadSpecies()
        {
            try
            {
                Stream stream = File.Open(DATA_FILENAME, FileMode.Open);
                BinaryFormatter bFormatter = new BinaryFormatter();

                Console.WriteLine("Reading species information.");
                this.listSpecies = (List<Species>)bFormatter.Deserialize(stream);
                stream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("The file that contains species information exists but there is a problem reading it.");
            }
        }
    }
}
