﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AFL
{
    public partial class FishermanListItem : UserControl
    {
        public FishermanListItem()
        {
            InitializeComponent();
        }

        public string FirstName { get; set; }
        public Button DeleteButton { get; set; }
        public Button EditButton { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public Image ProfilePicture { get; set; }

        public void SetButtons()
        {
            DeleteButton = buttonDelete;
            EditButton = buttonEdit;
        }

        public void ShowValues()
        {
            pictureBoxProfile.Image = ProfilePicture;
            labelFirstNameHere.Text = FirstName;
            labelLastNameHere.Text = LastName;
            labelBirthDateHere.Text = BirthDate.ToString("dd/MM/yyyy");
        }
    }
}
