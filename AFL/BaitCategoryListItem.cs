﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AFL
{
    public partial class BaitCategoryListItem : UserControl
    {
        public BaitCategoryListItem()
        {
            InitializeComponent();
        }

        public String Name { get; set; }
        public Button DeleteButton { get; set; }

        public void SetButtons()
        {
            DeleteButton = buttonDelete;
        }

        public void ShowValues()
        {
            labelNameHere.Text= Name;
        }

        private void BaitCategoryListItem_Load(object sender, EventArgs e)
        {

        }
    }
}
