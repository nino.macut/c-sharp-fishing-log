﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AFL
{
    public partial class LocationListItem : UserControl
    {
        public LocationListItem()
        {
            InitializeComponent();
        }

        public Image Image { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public Button DeleteButton { get; set; }
        public Button EditButton { get; set; }

        public void SetButtons()
        {
            DeleteButton = buttonDelete;
            EditButton = buttonEdit;
        }

        public void ShowValues()
        {
            pictureBoxImage.Image = Image;
            labelNameHere.Text = Name;
            richTextBoxDescription.Text = Description;
        }
    }
}
