﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AFL
{
    public partial class LocationAddForm : Form
    {
        public LocationAddForm()
        {
            InitializeComponent();
        }

        public Image Image { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            Image = pictureBoxImage.Image;
            Name = textBoxName.Text;
            Description = richTextBoxDescription.Text;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonUploadImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog chooseImage = new OpenFileDialog();
            chooseImage.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (chooseImage.ShowDialog() == DialogResult.OK)
            {
                pictureBoxImage.Image = new Bitmap(chooseImage.FileName);
            }
        }
    }
}
