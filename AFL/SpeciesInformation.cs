﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace AFL
{
    public class SpeciesInformation
    {
        public List<Species> listSpecies;

        private const String DATA_FILENAME = "speciesInformation.dat";

        public SpeciesInformation()
        {
            this.listSpecies = new List<Species>();
        }

        public void AddSpecies(Image image, String name, bool isPredator, String description)
        {
            foreach (Species s in listSpecies)
            {
                if (s.Name == name)
                {
                    Console.WriteLine("You had already added " + name + " before."); //CHANGE LATER
                    return;
                }
            }
            this.listSpecies.Add(new Species(image, name, isPredator, description));
            Console.WriteLine("Species succesfully added."); //CHANGE LATER
        }

        public void RemoveSpecies(String name)
        {
            foreach (Species s in listSpecies)
            {
                if (s.Name == name)
                {
                    if (this.listSpecies.Remove(s))
                    {
                        Console.WriteLine(name + " has been removed successfully."); //CHANGE LATER
                        return;
                    }
                    else
                    {
                        Console.WriteLine("Unable to remove " + name); //CHANGE LATER
                        return;
                    }
                }
            }
            Console.WriteLine(name + "had not been added before."); //CHANGE LATER
        }

        public void SaveSpecies()
        {
            try
            {
                Stream stream = File.Open(DATA_FILENAME, FileMode.Create);
                BinaryFormatter bFormatter = new BinaryFormatter();

                Console.WriteLine("Writing species information.");
                bFormatter.Serialize(stream, this.listSpecies);
                stream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("Unable to save species information.");
            }
        }

        public void LoadSpecies()
        {
            try
            {
                Stream stream = File.Open(DATA_FILENAME, FileMode.Open);
                BinaryFormatter bFormatter = new BinaryFormatter();

                Console.WriteLine("Reading species information.");
                this.listSpecies = (List<Species>)bFormatter.Deserialize(stream);
                stream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("The file that contains species information exists but there is a problem reading it.");
            }
        }
    }
}
