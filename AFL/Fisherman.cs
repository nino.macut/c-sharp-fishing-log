﻿using System;
using System.Drawing;

namespace AFL
{
    [Serializable]
    class Fisherman
    {
        public Fisherman(Image ProfilePicture, String FirstName, String LastName, DateTime BirthDate)
        {
            this.ProfilePicture = ProfilePicture;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.BirthDate = BirthDate;
        }

        public Image ProfilePicture { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
