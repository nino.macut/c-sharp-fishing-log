﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFL
{
    public abstract class LoadFileSystem
    {
        //Information handler.
        public BaitInformation baitInformation = new BaitInformation();
        public BaitCategoryInformation baitCategoryInformation = new BaitCategoryInformation();
        public CatchInformation catchInformation = new CatchInformation();
        public FishermanInformation fishermanInformation = new FishermanInformation();
        public LocationInformation locationInformation = new LocationInformation();
        public SpeciesInformation speciesInformation = new SpeciesInformation();
        public TripInformation tripInformation = new TripInformation();
        public LoadFileSystem() {
            LoadInformation();
        }

        //Loads all needed information.
        public virtual void LoadInformation()
        {
            baitInformation.LoadBait();
            baitCategoryInformation.LoadBaitCategories();
            catchInformation.LoadCatches();
            fishermanInformation.LoadFishermen();
            locationInformation.LoadLocations();
            speciesInformation.LoadSpecies();
            tripInformation.LoadTrips();
        }
    }
}
