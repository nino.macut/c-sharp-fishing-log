﻿namespace AFL
{
    partial class ListItemFisherman
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDelete = new System.Windows.Forms.Button();
            this.lblBirthDate = new System.Windows.Forms.Label();
            this.lblLastName = new System.Windows.Forms.Label();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.btnEdit = new System.Windows.Forms.Button();
            this.lblFirstNameHere = new System.Windows.Forms.Label();
            this.lblLastNameHere = new System.Windows.Forms.Label();
            this.lblBirthDateHere = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(437, 55);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(125, 35);
            this.btnDelete.TabIndex = 11;
            this.btnDelete.Text = "DELETE";
            this.btnDelete.UseVisualStyleBackColor = true;
            // 
            // lblBirthDate
            // 
            this.lblBirthDate.AutoSize = true;
            this.lblBirthDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBirthDate.ForeColor = System.Drawing.Color.White;
            this.lblBirthDate.Location = new System.Drawing.Point(19, 66);
            this.lblBirthDate.Name = "lblBirthDate";
            this.lblBirthDate.Size = new System.Drawing.Size(86, 13);
            this.lblBirthDate.TabIndex = 10;
            this.lblBirthDate.Text = "BIRTH DATE:";
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastName.ForeColor = System.Drawing.Color.White;
            this.lblLastName.Location = new System.Drawing.Point(19, 44);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(81, 13);
            this.lblLastName.TabIndex = 9;
            this.lblLastName.Text = "LAST NAME:";
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFirstName.ForeColor = System.Drawing.Color.White;
            this.lblFirstName.Location = new System.Drawing.Point(19, 22);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(86, 13);
            this.lblFirstName.TabIndex = 8;
            this.lblFirstName.Text = "FIRST NAME:";
            // 
            // btnEdit
            // 
            this.btnEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Location = new System.Drawing.Point(437, 11);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(125, 35);
            this.btnEdit.TabIndex = 12;
            this.btnEdit.Text = "EDIT";
            this.btnEdit.UseVisualStyleBackColor = true;
            // 
            // lblFirstNameHere
            // 
            this.lblFirstNameHere.AutoSize = true;
            this.lblFirstNameHere.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFirstNameHere.ForeColor = System.Drawing.Color.White;
            this.lblFirstNameHere.Location = new System.Drawing.Point(135, 21);
            this.lblFirstNameHere.Name = "lblFirstNameHere";
            this.lblFirstNameHere.Size = new System.Drawing.Size(129, 13);
            this.lblFirstNameHere.TabIndex = 13;
            this.lblFirstNameHere.Text = "First name goes here!";
            // 
            // lblLastNameHere
            // 
            this.lblLastNameHere.AutoSize = true;
            this.lblLastNameHere.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastNameHere.ForeColor = System.Drawing.Color.White;
            this.lblLastNameHere.Location = new System.Drawing.Point(135, 44);
            this.lblLastNameHere.Name = "lblLastNameHere";
            this.lblLastNameHere.Size = new System.Drawing.Size(129, 13);
            this.lblLastNameHere.TabIndex = 14;
            this.lblLastNameHere.Text = "Last name goes here!";
            // 
            // lblBirthDateHere
            // 
            this.lblBirthDateHere.AutoSize = true;
            this.lblBirthDateHere.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBirthDateHere.ForeColor = System.Drawing.Color.White;
            this.lblBirthDateHere.Location = new System.Drawing.Point(135, 66);
            this.lblBirthDateHere.Name = "lblBirthDateHere";
            this.lblBirthDateHere.Size = new System.Drawing.Size(126, 13);
            this.lblBirthDateHere.TabIndex = 15;
            this.lblBirthDateHere.Text = "Birth date goes here!";
            // 
            // ListItemFisherman
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(12)))), ((int)(((byte)(12)))));
            this.Controls.Add(this.lblBirthDateHere);
            this.Controls.Add(this.lblLastNameHere);
            this.Controls.Add(this.lblFirstNameHere);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.lblBirthDate);
            this.Controls.Add(this.lblLastName);
            this.Controls.Add(this.lblFirstName);
            this.Name = "ListItemFisherman";
            this.Size = new System.Drawing.Size(580, 100);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label lblBirthDate;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Label lblFirstNameHere;
        private System.Windows.Forms.Label lblLastNameHere;
        private System.Windows.Forms.Label lblBirthDateHere;
    }
}
