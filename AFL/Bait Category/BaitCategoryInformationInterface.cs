﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFL
{
    interface BaitCategoryInformationInterface
    {
        void AddBaitCategory(String name);
        void RemoveBaitCategory(String name);
        void SaveBaitCategories();
        void LoadBaitCategories();
    }
}
