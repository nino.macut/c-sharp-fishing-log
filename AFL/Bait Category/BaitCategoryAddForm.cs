﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AFL
{
    public partial class BaitCategoryAddForm : Form
    {
        public BaitCategoryAddForm()
        {
            InitializeComponent();
        }

        public String Name { get; set; }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            Name = textBoxName.Text;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
