﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFL
{
    [Serializable]
    public class BaitCategory
    {
        public BaitCategory(String Name)
        {
            this.Name = Name;
        }

        public String Name { get; set; }

        public override string ToString()
        {
            return ("BAIT CATEGORY:\nName: " + Name);
        }
    }
}
