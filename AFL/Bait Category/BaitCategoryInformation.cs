﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace AFL
{
    public class BaitCategoryInformation : BaitCategoryInformationInterface
    {
        public List<BaitCategory> listBaitCategories;

        //Bait category information will be saved to this file.
        private const String DATA_FILENAME = "baitCategoriesInfo.dat";

        public BaitCategoryInformation()
        {
            this.listBaitCategories = new List<BaitCategory>();
        }

        //Creates and adds a BaitCategory object to listBaitCategories.
        public void AddBaitCategory(String name)
        {
            foreach (BaitCategory b in listBaitCategories)
            {
                if (b.Name == name)
                {
                    Console.WriteLine("You had already added " + name + " before.");
                    return;
                }
            }
            this.listBaitCategories.Add(new BaitCategory(name));
            Console.WriteLine("Bait category succesfully added.");
        }

        //Removes a specific BaitCategory object from listBaitCategories.
        public void RemoveBaitCategory(String name)
        {
            foreach (BaitCategory b in listBaitCategories)
            {
                if (b.Name == name)
                {
                    if (this.listBaitCategories.Remove(b))
                    {
                        Console.WriteLine(name + " has been removed successfully.");
                        return;
                    }
                    else
                    {
                        Console.WriteLine("Unable to remove " + name);
                        return;
                    }
                }
            }
            Console.WriteLine(name + "had not been added before.");
        }

        //Saves BaitCategory information to appropriate file.
        public void SaveBaitCategories()
        {
            try
            {
                Stream stream = File.Open(DATA_FILENAME, FileMode.Create);
                BinaryFormatter bFormatter = new BinaryFormatter();

                Console.WriteLine("Writing bait category information.");
                bFormatter.Serialize(stream, this.listBaitCategories);
                stream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("Unable to save bait category information.");
            }
        }

        //Loads BaitCategory information from appropriate file
        public void LoadBaitCategories()
        {
            try
            {
                Stream stream = File.Open(DATA_FILENAME, FileMode.Open);
                BinaryFormatter bFormatter = new BinaryFormatter();

                Console.WriteLine("Reading bait category information.");
                this.listBaitCategories = (List<BaitCategory>)bFormatter.Deserialize(stream);
                stream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("The file that contains bait category information exists but there is a problem reading it.");
            }
        }
    }
}