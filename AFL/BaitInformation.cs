﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace AFL
{
    public class BaitInformation
    {
        public List<Bait> listBait;

        private const String DATA_FILENAME = "baitInformation.dat";

        public BaitInformation()
        {
            this.listBait = new List<Bait>();
        }

        public void AddBait(Image image, String category, String type, String name, Color color, String brand, String model, String description, bool hasRecipe, String recipe)
        {
            foreach (Bait b in listBait)
            {
                if (b.Name == name && b.Type == type)
                {
                    Console.WriteLine("You had already added " + name + " " + type + " before."); //CHANGE LATER
                    return;
                }
            }
            this.listBait.Add(new Bait(image, category, type, name, color, brand, model, description, hasRecipe, recipe));
            Console.WriteLine("Bait succesfully added."); //CHANGE LATER
        }

        public void RemoveBait(String category, String name)
        {
            foreach (Bait b in listBait)
            {
                if (b.Category == category && b.Name == name)
                {
                    if (this.listBait.Remove(b))
                    {
                        Console.WriteLine(category + " " + name + " has been removed successfully."); //CHANGE LATER
                        return;
                    }
                    else
                    {
                        Console.WriteLine("Unable to remove " + category + " " + name); //CHANGE LATER
                        return;
                    }
                }
            }
            Console.WriteLine(category + " " + name + "had not been added before."); //CHANGE LATER
        }

        public void SaveBait()
        {
            try
            {
                Stream stream = File.Open(DATA_FILENAME, FileMode.Create);
                BinaryFormatter bFormatter = new BinaryFormatter();

                Console.WriteLine("Writing bait information.");
                bFormatter.Serialize(stream, this.listBait);
                stream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("Unable to save bait information.");
            }
        }

        public void LoadBait()
        {
            try
            {
                Stream stream = File.Open(DATA_FILENAME, FileMode.Open);
                BinaryFormatter bFormatter = new BinaryFormatter();

                Console.WriteLine("Reading bait information.");
                this.listBait = (List<Bait>)bFormatter.Deserialize(stream);
                stream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("The file that contains bait information exists but there is a problem reading it.");
            }
        }
    }
}