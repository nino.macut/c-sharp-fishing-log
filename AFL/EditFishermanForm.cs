﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AFL
{
    public partial class EditFishermanForm : Form
    {
        public EditFishermanForm()
        {
            InitializeComponent();
        }

        private string firstName;
        private string lastName;
        private DateTime birthDate;

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }

        public void ShowValues()
        {
            tbFirstName.Text = FirstName;
            tbLastName.Text = LastName;
            dtpBirthDate.Value = BirthDate;
        }

        //On click set values and close form.
        private void btnEdit_MouseClick(object sender, MouseEventArgs e)
        {
            FirstName = tbFirstName.Text;
            LastName = tbLastName.Text;
            BirthDate = dtpBirthDate.Value;
            this.Close();
        }

        //On click close form.
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
