﻿namespace AFL
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.panelSideMenu = new System.Windows.Forms.Panel();
            this.panelSpeciesSubmenu = new System.Windows.Forms.Panel();
            this.buttonAddSpecies = new System.Windows.Forms.Button();
            this.buttonViewSpecies = new System.Windows.Forms.Button();
            this.buttonSpecies = new System.Windows.Forms.Button();
            this.panelSeperator = new System.Windows.Forms.Panel();
            this.panelCatchesSubmenu = new System.Windows.Forms.Panel();
            this.buttonCatchesAdd = new System.Windows.Forms.Button();
            this.buttonCatchesView = new System.Windows.Forms.Button();
            this.buttonCatches = new System.Windows.Forms.Button();
            this.panelBaitsSubmenu = new System.Windows.Forms.Panel();
            this.buttonBaitsAdd = new System.Windows.Forms.Button();
            this.buttonBaitsView = new System.Windows.Forms.Button();
            this.buttonBaitsAddCategory = new System.Windows.Forms.Button();
            this.buttonBaitsViewCategories = new System.Windows.Forms.Button();
            this.buttonBaits = new System.Windows.Forms.Button();
            this.panelTripsSubmenu = new System.Windows.Forms.Panel();
            this.buttonTripsPlan = new System.Windows.Forms.Button();
            this.buttonTripsView = new System.Windows.Forms.Button();
            this.buttonTrips = new System.Windows.Forms.Button();
            this.panelLocationsSubmenu = new System.Windows.Forms.Panel();
            this.buttonLocationsAdd = new System.Windows.Forms.Button();
            this.buttonLocationsView = new System.Windows.Forms.Button();
            this.buttonLocations = new System.Windows.Forms.Button();
            this.panelFishermenSubmenu = new System.Windows.Forms.Panel();
            this.buttonFishermenAdd = new System.Windows.Forms.Button();
            this.buttonFishermenView = new System.Windows.Forms.Button();
            this.buttonFishermen = new System.Windows.Forms.Button();
            this.panelMenuLogo = new System.Windows.Forms.Panel();
            this.pictureBoxMenuLogo = new System.Windows.Forms.PictureBox();
            this.panelControls = new System.Windows.Forms.Panel();
            this.buttonMinimize = new System.Windows.Forms.Button();
            this.buttonToggle = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelMain = new System.Windows.Forms.Panel();
            this.panelContainer = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.labelCurrentTime = new System.Windows.Forms.Label();
            this.panelSideMenu.SuspendLayout();
            this.panelSpeciesSubmenu.SuspendLayout();
            this.panelCatchesSubmenu.SuspendLayout();
            this.panelBaitsSubmenu.SuspendLayout();
            this.panelTripsSubmenu.SuspendLayout();
            this.panelLocationsSubmenu.SuspendLayout();
            this.panelFishermenSubmenu.SuspendLayout();
            this.panelMenuLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMenuLogo)).BeginInit();
            this.panelControls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelMain.SuspendLayout();
            this.panelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // panelSideMenu
            // 
            this.panelSideMenu.AutoScroll = true;
            this.panelSideMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.panelSideMenu.Controls.Add(this.panelSpeciesSubmenu);
            this.panelSideMenu.Controls.Add(this.buttonSpecies);
            this.panelSideMenu.Controls.Add(this.panelSeperator);
            this.panelSideMenu.Controls.Add(this.panelCatchesSubmenu);
            this.panelSideMenu.Controls.Add(this.buttonCatches);
            this.panelSideMenu.Controls.Add(this.panelBaitsSubmenu);
            this.panelSideMenu.Controls.Add(this.buttonBaits);
            this.panelSideMenu.Controls.Add(this.panelTripsSubmenu);
            this.panelSideMenu.Controls.Add(this.buttonTrips);
            this.panelSideMenu.Controls.Add(this.panelLocationsSubmenu);
            this.panelSideMenu.Controls.Add(this.buttonLocations);
            this.panelSideMenu.Controls.Add(this.panelFishermenSubmenu);
            this.panelSideMenu.Controls.Add(this.buttonFishermen);
            this.panelSideMenu.Controls.Add(this.panelMenuLogo);
            this.panelSideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSideMenu.Location = new System.Drawing.Point(0, 0);
            this.panelSideMenu.Name = "panelSideMenu";
            this.panelSideMenu.Size = new System.Drawing.Size(250, 570);
            this.panelSideMenu.TabIndex = 0;
            // 
            // panelSpeciesSubmenu
            // 
            this.panelSpeciesSubmenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.panelSpeciesSubmenu.Controls.Add(this.buttonAddSpecies);
            this.panelSpeciesSubmenu.Controls.Add(this.buttonViewSpecies);
            this.panelSpeciesSubmenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSpeciesSubmenu.Location = new System.Drawing.Point(0, 852);
            this.panelSpeciesSubmenu.Name = "panelSpeciesSubmenu";
            this.panelSpeciesSubmenu.Size = new System.Drawing.Size(233, 80);
            this.panelSpeciesSubmenu.TabIndex = 14;
            // 
            // buttonAddSpecies
            // 
            this.buttonAddSpecies.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonAddSpecies.FlatAppearance.BorderSize = 0;
            this.buttonAddSpecies.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddSpecies.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddSpecies.ForeColor = System.Drawing.Color.LightGray;
            this.buttonAddSpecies.Location = new System.Drawing.Point(0, 40);
            this.buttonAddSpecies.Name = "buttonAddSpecies";
            this.buttonAddSpecies.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.buttonAddSpecies.Size = new System.Drawing.Size(233, 40);
            this.buttonAddSpecies.TabIndex = 1;
            this.buttonAddSpecies.Text = "Add Species";
            this.buttonAddSpecies.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAddSpecies.UseVisualStyleBackColor = true;
            this.buttonAddSpecies.Click += new System.EventHandler(this.buttonAddSpecies_Click);
            // 
            // buttonViewSpecies
            // 
            this.buttonViewSpecies.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonViewSpecies.FlatAppearance.BorderSize = 0;
            this.buttonViewSpecies.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonViewSpecies.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonViewSpecies.ForeColor = System.Drawing.Color.LightGray;
            this.buttonViewSpecies.Location = new System.Drawing.Point(0, 0);
            this.buttonViewSpecies.Name = "buttonViewSpecies";
            this.buttonViewSpecies.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.buttonViewSpecies.Size = new System.Drawing.Size(233, 40);
            this.buttonViewSpecies.TabIndex = 0;
            this.buttonViewSpecies.Text = "View Species";
            this.buttonViewSpecies.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonViewSpecies.UseVisualStyleBackColor = true;
            this.buttonViewSpecies.Click += new System.EventHandler(this.buttonViewSpecies_Click);
            // 
            // buttonSpecies
            // 
            this.buttonSpecies.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.buttonSpecies.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonSpecies.FlatAppearance.BorderSize = 0;
            this.buttonSpecies.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(23)))), ((int)(((byte)(38)))));
            this.buttonSpecies.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSpecies.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSpecies.ForeColor = System.Drawing.Color.Gainsboro;
            this.buttonSpecies.Image = global::AFL.Properties.Resources.speciesIconPink;
            this.buttonSpecies.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSpecies.Location = new System.Drawing.Point(0, 807);
            this.buttonSpecies.Name = "buttonSpecies";
            this.buttonSpecies.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.buttonSpecies.Size = new System.Drawing.Size(233, 45);
            this.buttonSpecies.TabIndex = 13;
            this.buttonSpecies.Text = "       SPECIES";
            this.buttonSpecies.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSpecies.UseVisualStyleBackColor = false;
            this.buttonSpecies.Click += new System.EventHandler(this.buttonSpecies_Click);
            // 
            // panelSeperator
            // 
            this.panelSeperator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(43)))), ((int)(((byte)(80)))));
            this.panelSeperator.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSeperator.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(43)))), ((int)(((byte)(80)))));
            this.panelSeperator.Location = new System.Drawing.Point(0, 805);
            this.panelSeperator.Name = "panelSeperator";
            this.panelSeperator.Size = new System.Drawing.Size(233, 2);
            this.panelSeperator.TabIndex = 12;
            // 
            // panelCatchesSubmenu
            // 
            this.panelCatchesSubmenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.panelCatchesSubmenu.Controls.Add(this.buttonCatchesAdd);
            this.panelCatchesSubmenu.Controls.Add(this.buttonCatchesView);
            this.panelCatchesSubmenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCatchesSubmenu.Location = new System.Drawing.Point(0, 725);
            this.panelCatchesSubmenu.Name = "panelCatchesSubmenu";
            this.panelCatchesSubmenu.Size = new System.Drawing.Size(233, 80);
            this.panelCatchesSubmenu.TabIndex = 10;
            // 
            // buttonCatchesAdd
            // 
            this.buttonCatchesAdd.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonCatchesAdd.FlatAppearance.BorderSize = 0;
            this.buttonCatchesAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCatchesAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCatchesAdd.ForeColor = System.Drawing.Color.LightGray;
            this.buttonCatchesAdd.Location = new System.Drawing.Point(0, 40);
            this.buttonCatchesAdd.Name = "buttonCatchesAdd";
            this.buttonCatchesAdd.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.buttonCatchesAdd.Size = new System.Drawing.Size(233, 40);
            this.buttonCatchesAdd.TabIndex = 1;
            this.buttonCatchesAdd.Text = "Add Catch";
            this.buttonCatchesAdd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonCatchesAdd.UseVisualStyleBackColor = true;
            this.buttonCatchesAdd.Click += new System.EventHandler(this.buttonCatchesAdd_Click);
            // 
            // buttonCatchesView
            // 
            this.buttonCatchesView.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonCatchesView.FlatAppearance.BorderSize = 0;
            this.buttonCatchesView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCatchesView.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCatchesView.ForeColor = System.Drawing.Color.LightGray;
            this.buttonCatchesView.Location = new System.Drawing.Point(0, 0);
            this.buttonCatchesView.Name = "buttonCatchesView";
            this.buttonCatchesView.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.buttonCatchesView.Size = new System.Drawing.Size(233, 40);
            this.buttonCatchesView.TabIndex = 0;
            this.buttonCatchesView.Text = "View Catches";
            this.buttonCatchesView.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonCatchesView.UseVisualStyleBackColor = true;
            this.buttonCatchesView.Click += new System.EventHandler(this.buttonCatchesView_Click);
            // 
            // buttonCatches
            // 
            this.buttonCatches.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.buttonCatches.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonCatches.FlatAppearance.BorderSize = 0;
            this.buttonCatches.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(23)))), ((int)(((byte)(38)))));
            this.buttonCatches.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCatches.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCatches.ForeColor = System.Drawing.Color.Gainsboro;
            this.buttonCatches.Image = ((System.Drawing.Image)(resources.GetObject("buttonCatches.Image")));
            this.buttonCatches.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonCatches.Location = new System.Drawing.Point(0, 680);
            this.buttonCatches.Name = "buttonCatches";
            this.buttonCatches.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.buttonCatches.Size = new System.Drawing.Size(233, 45);
            this.buttonCatches.TabIndex = 9;
            this.buttonCatches.Text = "       CATCHES";
            this.buttonCatches.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonCatches.UseVisualStyleBackColor = false;
            this.buttonCatches.Click += new System.EventHandler(this.buttonCatches_Click);
            // 
            // panelBaitsSubmenu
            // 
            this.panelBaitsSubmenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.panelBaitsSubmenu.Controls.Add(this.buttonBaitsAdd);
            this.panelBaitsSubmenu.Controls.Add(this.buttonBaitsView);
            this.panelBaitsSubmenu.Controls.Add(this.buttonBaitsAddCategory);
            this.panelBaitsSubmenu.Controls.Add(this.buttonBaitsViewCategories);
            this.panelBaitsSubmenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelBaitsSubmenu.Location = new System.Drawing.Point(0, 520);
            this.panelBaitsSubmenu.Name = "panelBaitsSubmenu";
            this.panelBaitsSubmenu.Size = new System.Drawing.Size(233, 160);
            this.panelBaitsSubmenu.TabIndex = 8;
            // 
            // buttonBaitsAdd
            // 
            this.buttonBaitsAdd.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonBaitsAdd.FlatAppearance.BorderSize = 0;
            this.buttonBaitsAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBaitsAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBaitsAdd.ForeColor = System.Drawing.Color.LightGray;
            this.buttonBaitsAdd.Location = new System.Drawing.Point(0, 120);
            this.buttonBaitsAdd.Name = "buttonBaitsAdd";
            this.buttonBaitsAdd.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.buttonBaitsAdd.Size = new System.Drawing.Size(233, 40);
            this.buttonBaitsAdd.TabIndex = 3;
            this.buttonBaitsAdd.Text = "Add Bait";
            this.buttonBaitsAdd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonBaitsAdd.UseVisualStyleBackColor = true;
            this.buttonBaitsAdd.Click += new System.EventHandler(this.buttonBaitsAdd_Click);
            // 
            // buttonBaitsView
            // 
            this.buttonBaitsView.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonBaitsView.FlatAppearance.BorderSize = 0;
            this.buttonBaitsView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBaitsView.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBaitsView.ForeColor = System.Drawing.Color.LightGray;
            this.buttonBaitsView.Location = new System.Drawing.Point(0, 80);
            this.buttonBaitsView.Name = "buttonBaitsView";
            this.buttonBaitsView.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.buttonBaitsView.Size = new System.Drawing.Size(233, 40);
            this.buttonBaitsView.TabIndex = 2;
            this.buttonBaitsView.Text = "View Baits";
            this.buttonBaitsView.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonBaitsView.UseVisualStyleBackColor = true;
            this.buttonBaitsView.Click += new System.EventHandler(this.buttonBaitsView_Click);
            // 
            // buttonBaitsAddCategory
            // 
            this.buttonBaitsAddCategory.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonBaitsAddCategory.FlatAppearance.BorderSize = 0;
            this.buttonBaitsAddCategory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBaitsAddCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBaitsAddCategory.ForeColor = System.Drawing.Color.LightGray;
            this.buttonBaitsAddCategory.Location = new System.Drawing.Point(0, 40);
            this.buttonBaitsAddCategory.Name = "buttonBaitsAddCategory";
            this.buttonBaitsAddCategory.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.buttonBaitsAddCategory.Size = new System.Drawing.Size(233, 40);
            this.buttonBaitsAddCategory.TabIndex = 1;
            this.buttonBaitsAddCategory.Text = "Add Bait Category";
            this.buttonBaitsAddCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonBaitsAddCategory.UseVisualStyleBackColor = true;
            this.buttonBaitsAddCategory.Click += new System.EventHandler(this.buttonBaitsAddCategory_Click);
            // 
            // buttonBaitsViewCategories
            // 
            this.buttonBaitsViewCategories.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonBaitsViewCategories.FlatAppearance.BorderSize = 0;
            this.buttonBaitsViewCategories.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBaitsViewCategories.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBaitsViewCategories.ForeColor = System.Drawing.Color.LightGray;
            this.buttonBaitsViewCategories.Location = new System.Drawing.Point(0, 0);
            this.buttonBaitsViewCategories.Name = "buttonBaitsViewCategories";
            this.buttonBaitsViewCategories.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.buttonBaitsViewCategories.Size = new System.Drawing.Size(233, 40);
            this.buttonBaitsViewCategories.TabIndex = 0;
            this.buttonBaitsViewCategories.Text = "View Bait Categories";
            this.buttonBaitsViewCategories.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonBaitsViewCategories.UseVisualStyleBackColor = true;
            this.buttonBaitsViewCategories.Click += new System.EventHandler(this.buttonBaitsViewCategories_Click);
            // 
            // buttonBaits
            // 
            this.buttonBaits.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.buttonBaits.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonBaits.FlatAppearance.BorderSize = 0;
            this.buttonBaits.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(23)))), ((int)(((byte)(38)))));
            this.buttonBaits.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBaits.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBaits.ForeColor = System.Drawing.Color.Gainsboro;
            this.buttonBaits.Image = global::AFL.Properties.Resources.baitsIconPink;
            this.buttonBaits.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonBaits.Location = new System.Drawing.Point(0, 475);
            this.buttonBaits.Name = "buttonBaits";
            this.buttonBaits.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.buttonBaits.Size = new System.Drawing.Size(233, 45);
            this.buttonBaits.TabIndex = 7;
            this.buttonBaits.Text = "       BAITS";
            this.buttonBaits.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonBaits.UseVisualStyleBackColor = false;
            this.buttonBaits.Click += new System.EventHandler(this.buttonBaits_Click);
            // 
            // panelTripsSubmenu
            // 
            this.panelTripsSubmenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.panelTripsSubmenu.Controls.Add(this.buttonTripsPlan);
            this.panelTripsSubmenu.Controls.Add(this.buttonTripsView);
            this.panelTripsSubmenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTripsSubmenu.Location = new System.Drawing.Point(0, 395);
            this.panelTripsSubmenu.Name = "panelTripsSubmenu";
            this.panelTripsSubmenu.Size = new System.Drawing.Size(233, 80);
            this.panelTripsSubmenu.TabIndex = 6;
            // 
            // buttonTripsPlan
            // 
            this.buttonTripsPlan.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonTripsPlan.FlatAppearance.BorderSize = 0;
            this.buttonTripsPlan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTripsPlan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTripsPlan.ForeColor = System.Drawing.Color.LightGray;
            this.buttonTripsPlan.Location = new System.Drawing.Point(0, 40);
            this.buttonTripsPlan.Name = "buttonTripsPlan";
            this.buttonTripsPlan.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.buttonTripsPlan.Size = new System.Drawing.Size(233, 40);
            this.buttonTripsPlan.TabIndex = 1;
            this.buttonTripsPlan.Text = "Plan Trip";
            this.buttonTripsPlan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonTripsPlan.UseVisualStyleBackColor = true;
            this.buttonTripsPlan.Click += new System.EventHandler(this.buttonTripsPlan_Click);
            // 
            // buttonTripsView
            // 
            this.buttonTripsView.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonTripsView.FlatAppearance.BorderSize = 0;
            this.buttonTripsView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTripsView.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTripsView.ForeColor = System.Drawing.Color.LightGray;
            this.buttonTripsView.Location = new System.Drawing.Point(0, 0);
            this.buttonTripsView.Name = "buttonTripsView";
            this.buttonTripsView.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.buttonTripsView.Size = new System.Drawing.Size(233, 40);
            this.buttonTripsView.TabIndex = 0;
            this.buttonTripsView.Text = "View Trips";
            this.buttonTripsView.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonTripsView.UseVisualStyleBackColor = true;
            this.buttonTripsView.Click += new System.EventHandler(this.buttonTripsView_Click);
            // 
            // buttonTrips
            // 
            this.buttonTrips.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.buttonTrips.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonTrips.FlatAppearance.BorderSize = 0;
            this.buttonTrips.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(23)))), ((int)(((byte)(38)))));
            this.buttonTrips.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTrips.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTrips.ForeColor = System.Drawing.Color.Gainsboro;
            this.buttonTrips.Image = global::AFL.Properties.Resources.tripsIconPink;
            this.buttonTrips.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonTrips.Location = new System.Drawing.Point(0, 350);
            this.buttonTrips.Name = "buttonTrips";
            this.buttonTrips.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.buttonTrips.Size = new System.Drawing.Size(233, 45);
            this.buttonTrips.TabIndex = 5;
            this.buttonTrips.Text = "       TRIPS";
            this.buttonTrips.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonTrips.UseVisualStyleBackColor = false;
            this.buttonTrips.Click += new System.EventHandler(this.buttonTrips_Click);
            // 
            // panelLocationsSubmenu
            // 
            this.panelLocationsSubmenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.panelLocationsSubmenu.Controls.Add(this.buttonLocationsAdd);
            this.panelLocationsSubmenu.Controls.Add(this.buttonLocationsView);
            this.panelLocationsSubmenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLocationsSubmenu.Location = new System.Drawing.Point(0, 270);
            this.panelLocationsSubmenu.Name = "panelLocationsSubmenu";
            this.panelLocationsSubmenu.Size = new System.Drawing.Size(233, 80);
            this.panelLocationsSubmenu.TabIndex = 4;
            // 
            // buttonLocationsAdd
            // 
            this.buttonLocationsAdd.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonLocationsAdd.FlatAppearance.BorderSize = 0;
            this.buttonLocationsAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLocationsAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLocationsAdd.ForeColor = System.Drawing.Color.LightGray;
            this.buttonLocationsAdd.Location = new System.Drawing.Point(0, 40);
            this.buttonLocationsAdd.Name = "buttonLocationsAdd";
            this.buttonLocationsAdd.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.buttonLocationsAdd.Size = new System.Drawing.Size(233, 40);
            this.buttonLocationsAdd.TabIndex = 1;
            this.buttonLocationsAdd.Text = "Add Location";
            this.buttonLocationsAdd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonLocationsAdd.UseVisualStyleBackColor = true;
            this.buttonLocationsAdd.Click += new System.EventHandler(this.buttonLocationsAdd_Click);
            // 
            // buttonLocationsView
            // 
            this.buttonLocationsView.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonLocationsView.FlatAppearance.BorderSize = 0;
            this.buttonLocationsView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLocationsView.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLocationsView.ForeColor = System.Drawing.Color.LightGray;
            this.buttonLocationsView.Location = new System.Drawing.Point(0, 0);
            this.buttonLocationsView.Name = "buttonLocationsView";
            this.buttonLocationsView.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.buttonLocationsView.Size = new System.Drawing.Size(233, 40);
            this.buttonLocationsView.TabIndex = 0;
            this.buttonLocationsView.Text = "View Locations";
            this.buttonLocationsView.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonLocationsView.UseVisualStyleBackColor = true;
            this.buttonLocationsView.Click += new System.EventHandler(this.buttonLocationsView_Click);
            // 
            // buttonLocations
            // 
            this.buttonLocations.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.buttonLocations.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonLocations.FlatAppearance.BorderSize = 0;
            this.buttonLocations.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(23)))), ((int)(((byte)(38)))));
            this.buttonLocations.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLocations.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLocations.ForeColor = System.Drawing.Color.Gainsboro;
            this.buttonLocations.Image = global::AFL.Properties.Resources.locationsIconPink;
            this.buttonLocations.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonLocations.Location = new System.Drawing.Point(0, 225);
            this.buttonLocations.Name = "buttonLocations";
            this.buttonLocations.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.buttonLocations.Size = new System.Drawing.Size(233, 45);
            this.buttonLocations.TabIndex = 3;
            this.buttonLocations.Text = "       LOCATIONS";
            this.buttonLocations.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonLocations.UseVisualStyleBackColor = false;
            this.buttonLocations.Click += new System.EventHandler(this.buttonLocations_Click);
            // 
            // panelFishermenSubmenu
            // 
            this.panelFishermenSubmenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.panelFishermenSubmenu.Controls.Add(this.buttonFishermenAdd);
            this.panelFishermenSubmenu.Controls.Add(this.buttonFishermenView);
            this.panelFishermenSubmenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelFishermenSubmenu.Location = new System.Drawing.Point(0, 145);
            this.panelFishermenSubmenu.Name = "panelFishermenSubmenu";
            this.panelFishermenSubmenu.Size = new System.Drawing.Size(233, 80);
            this.panelFishermenSubmenu.TabIndex = 2;
            // 
            // buttonFishermenAdd
            // 
            this.buttonFishermenAdd.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonFishermenAdd.FlatAppearance.BorderSize = 0;
            this.buttonFishermenAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFishermenAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonFishermenAdd.ForeColor = System.Drawing.Color.LightGray;
            this.buttonFishermenAdd.Location = new System.Drawing.Point(0, 40);
            this.buttonFishermenAdd.Name = "buttonFishermenAdd";
            this.buttonFishermenAdd.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.buttonFishermenAdd.Size = new System.Drawing.Size(233, 40);
            this.buttonFishermenAdd.TabIndex = 1;
            this.buttonFishermenAdd.Text = "Add Fishermen";
            this.buttonFishermenAdd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonFishermenAdd.UseVisualStyleBackColor = true;
            this.buttonFishermenAdd.Click += new System.EventHandler(this.buttonFishermenAdd_Click);
            // 
            // buttonFishermenView
            // 
            this.buttonFishermenView.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonFishermenView.FlatAppearance.BorderSize = 0;
            this.buttonFishermenView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFishermenView.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonFishermenView.ForeColor = System.Drawing.Color.LightGray;
            this.buttonFishermenView.Location = new System.Drawing.Point(0, 0);
            this.buttonFishermenView.Name = "buttonFishermenView";
            this.buttonFishermenView.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.buttonFishermenView.Size = new System.Drawing.Size(233, 40);
            this.buttonFishermenView.TabIndex = 0;
            this.buttonFishermenView.Text = "View Fishermen";
            this.buttonFishermenView.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonFishermenView.UseVisualStyleBackColor = true;
            this.buttonFishermenView.Click += new System.EventHandler(this.buttonFishermenView_Click);
            // 
            // buttonFishermen
            // 
            this.buttonFishermen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.buttonFishermen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonFishermen.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonFishermen.FlatAppearance.BorderSize = 0;
            this.buttonFishermen.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(23)))), ((int)(((byte)(38)))));
            this.buttonFishermen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFishermen.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonFishermen.ForeColor = System.Drawing.Color.Gainsboro;
            this.buttonFishermen.Image = global::AFL.Properties.Resources.fishermenIconPink;
            this.buttonFishermen.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonFishermen.Location = new System.Drawing.Point(0, 100);
            this.buttonFishermen.Name = "buttonFishermen";
            this.buttonFishermen.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.buttonFishermen.Size = new System.Drawing.Size(233, 45);
            this.buttonFishermen.TabIndex = 1;
            this.buttonFishermen.Text = "       FISHERMEN";
            this.buttonFishermen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonFishermen.UseVisualStyleBackColor = false;
            this.buttonFishermen.Click += new System.EventHandler(this.buttonFishermen_Click);
            // 
            // panelMenuLogo
            // 
            this.panelMenuLogo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.panelMenuLogo.Controls.Add(this.pictureBoxMenuLogo);
            this.panelMenuLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelMenuLogo.Location = new System.Drawing.Point(0, 0);
            this.panelMenuLogo.Name = "panelMenuLogo";
            this.panelMenuLogo.Size = new System.Drawing.Size(233, 100);
            this.panelMenuLogo.TabIndex = 0;
            // 
            // pictureBoxMenuLogo
            // 
            this.pictureBoxMenuLogo.Image = global::AFL.Properties.Resources.logoWithTextReverse;
            this.pictureBoxMenuLogo.Location = new System.Drawing.Point(16, -50);
            this.pictureBoxMenuLogo.Name = "pictureBoxMenuLogo";
            this.pictureBoxMenuLogo.Size = new System.Drawing.Size(200, 200);
            this.pictureBoxMenuLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxMenuLogo.TabIndex = 0;
            this.pictureBoxMenuLogo.TabStop = false;
            // 
            // panelControls
            // 
            this.panelControls.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.panelControls.Controls.Add(this.labelCurrentTime);
            this.panelControls.Controls.Add(this.buttonMinimize);
            this.panelControls.Controls.Add(this.buttonToggle);
            this.panelControls.Controls.Add(this.buttonClose);
            this.panelControls.Controls.Add(this.pictureBox1);
            this.panelControls.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControls.Location = new System.Drawing.Point(0, 0);
            this.panelControls.Name = "panelControls";
            this.panelControls.Size = new System.Drawing.Size(950, 30);
            this.panelControls.TabIndex = 1;
            // 
            // buttonMinimize
            // 
            this.buttonMinimize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.buttonMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonMinimize.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonMinimize.FlatAppearance.BorderSize = 0;
            this.buttonMinimize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(23)))), ((int)(((byte)(38)))));
            this.buttonMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMinimize.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMinimize.ForeColor = System.Drawing.Color.Gainsboro;
            this.buttonMinimize.Image = global::AFL.Properties.Resources.minimizeToTaskBarIconPink;
            this.buttonMinimize.Location = new System.Drawing.Point(800, 0);
            this.buttonMinimize.Name = "buttonMinimize";
            this.buttonMinimize.Size = new System.Drawing.Size(50, 30);
            this.buttonMinimize.TabIndex = 4;
            this.buttonMinimize.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonMinimize.UseVisualStyleBackColor = false;
            this.buttonMinimize.Click += new System.EventHandler(this.buttonMinimize_Click);
            // 
            // buttonToggle
            // 
            this.buttonToggle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.buttonToggle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonToggle.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonToggle.FlatAppearance.BorderSize = 0;
            this.buttonToggle.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(23)))), ((int)(((byte)(38)))));
            this.buttonToggle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonToggle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonToggle.ForeColor = System.Drawing.Color.Gainsboro;
            this.buttonToggle.Image = global::AFL.Properties.Resources.maximizeIconPink;
            this.buttonToggle.Location = new System.Drawing.Point(850, 0);
            this.buttonToggle.Name = "buttonToggle";
            this.buttonToggle.Size = new System.Drawing.Size(50, 30);
            this.buttonToggle.TabIndex = 3;
            this.buttonToggle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonToggle.UseVisualStyleBackColor = false;
            this.buttonToggle.Click += new System.EventHandler(this.buttonToggle_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(7)))), ((int)(((byte)(21)))));
            this.buttonClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonClose.FlatAppearance.BorderSize = 0;
            this.buttonClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(23)))), ((int)(((byte)(38)))));
            this.buttonClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClose.ForeColor = System.Drawing.Color.Gainsboro;
            this.buttonClose.Image = global::AFL.Properties.Resources.closeIconPink;
            this.buttonClose.Location = new System.Drawing.Point(900, 0);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(50, 30);
            this.buttonClose.TabIndex = 2;
            this.buttonClose.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonClose.UseVisualStyleBackColor = false;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = global::AFL.Properties.Resources.logo;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(30, 30);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.panelContainer);
            this.panelMain.Controls.Add(this.panelSideMenu);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(0, 30);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(950, 570);
            this.panelMain.TabIndex = 2;
            // 
            // panelContainer
            // 
            this.panelContainer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(31)))), ((int)(((byte)(46)))));
            this.panelContainer.Controls.Add(this.pictureBox2);
            this.panelContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelContainer.Location = new System.Drawing.Point(250, 0);
            this.panelContainer.Name = "panelContainer";
            this.panelContainer.Size = new System.Drawing.Size(700, 570);
            this.panelContainer.TabIndex = 1;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.Image = global::AFL.Properties.Resources.mainLogo;
            this.pictureBox2.Location = new System.Drawing.Point(134, 135);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(433, 300);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // labelCurrentTime
            // 
            this.labelCurrentTime.AutoSize = true;
            this.labelCurrentTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCurrentTime.ForeColor = System.Drawing.Color.Gainsboro;
            this.labelCurrentTime.Location = new System.Drawing.Point(45, 9);
            this.labelCurrentTime.Name = "labelCurrentTime";
            this.labelCurrentTime.Size = new System.Drawing.Size(101, 13);
            this.labelCurrentTime.TabIndex = 5;
            this.labelCurrentTime.Text = "CURRENT TIME";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(950, 600);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.panelControls);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(950, 600);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main";
            this.panelSideMenu.ResumeLayout(false);
            this.panelSpeciesSubmenu.ResumeLayout(false);
            this.panelCatchesSubmenu.ResumeLayout(false);
            this.panelBaitsSubmenu.ResumeLayout(false);
            this.panelTripsSubmenu.ResumeLayout(false);
            this.panelLocationsSubmenu.ResumeLayout(false);
            this.panelFishermenSubmenu.ResumeLayout(false);
            this.panelMenuLogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMenuLogo)).EndInit();
            this.panelControls.ResumeLayout(false);
            this.panelControls.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelMain.ResumeLayout(false);
            this.panelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelSideMenu;
        private System.Windows.Forms.Button buttonCatches;
        private System.Windows.Forms.Panel panelBaitsSubmenu;
        private System.Windows.Forms.Button buttonBaitsAdd;
        private System.Windows.Forms.Button buttonBaitsView;
        private System.Windows.Forms.Button buttonBaitsAddCategory;
        private System.Windows.Forms.Button buttonBaitsViewCategories;
        private System.Windows.Forms.Button buttonBaits;
        private System.Windows.Forms.Panel panelTripsSubmenu;
        private System.Windows.Forms.Button buttonTripsPlan;
        private System.Windows.Forms.Button buttonTripsView;
        private System.Windows.Forms.Button buttonTrips;
        private System.Windows.Forms.Panel panelLocationsSubmenu;
        private System.Windows.Forms.Button buttonLocationsAdd;
        private System.Windows.Forms.Button buttonLocationsView;
        private System.Windows.Forms.Button buttonLocations;
        private System.Windows.Forms.Panel panelFishermenSubmenu;
        private System.Windows.Forms.Button buttonFishermenAdd;
        private System.Windows.Forms.Button buttonFishermenView;
        private System.Windows.Forms.Button buttonFishermen;
        private System.Windows.Forms.Panel panelMenuLogo;
        private System.Windows.Forms.Panel panelCatchesSubmenu;
        private System.Windows.Forms.Button buttonCatchesAdd;
        private System.Windows.Forms.Button buttonCatchesView;
        private System.Windows.Forms.Panel panelSeperator;
        private System.Windows.Forms.Panel panelControls;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.Panel panelContainer;
        private System.Windows.Forms.PictureBox pictureBoxMenuLogo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonToggle;
        private System.Windows.Forms.Button buttonMinimize;
        private System.Windows.Forms.Button buttonSpecies;
        private System.Windows.Forms.Panel panelSpeciesSubmenu;
        private System.Windows.Forms.Button buttonAddSpecies;
        private System.Windows.Forms.Button buttonViewSpecies;
        private System.Windows.Forms.Label labelCurrentTime;
    }
}

