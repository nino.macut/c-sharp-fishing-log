﻿namespace AFL
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.pnlSeparator2 = new System.Windows.Forms.Panel();
            this.lblLocations = new System.Windows.Forms.Label();
            this.pbLocations = new System.Windows.Forms.PictureBox();
            this.lblBait = new System.Windows.Forms.Label();
            this.lblCatches = new System.Windows.Forms.Label();
            this.lblTrips = new System.Windows.Forms.Label();
            this.lblFishermen = new System.Windows.Forms.Label();
            this.pnlSeparator1 = new System.Windows.Forms.Panel();
            this.pbBait = new System.Windows.Forms.PictureBox();
            this.pbCatches = new System.Windows.Forms.PictureBox();
            this.pbTrips = new System.Windows.Forms.PictureBox();
            this.pbFishermen = new System.Windows.Forms.PictureBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.flpFishermen = new System.Windows.Forms.FlowLayoutPanel();
            this.lblFishermenTitle = new System.Windows.Forms.Label();
            this.btnAddFisherman = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlFishermen = new System.Windows.Forms.Panel();
            this.pnlMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLocations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBait)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCatches)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTrips)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFishermen)).BeginInit();
            this.pnlFishermen.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMenu
            // 
            this.pnlMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(12)))), ((int)(((byte)(12)))));
            this.pnlMenu.Controls.Add(this.pnlSeparator2);
            this.pnlMenu.Controls.Add(this.lblLocations);
            this.pnlMenu.Controls.Add(this.pbLocations);
            this.pnlMenu.Controls.Add(this.lblBait);
            this.pnlMenu.Controls.Add(this.lblCatches);
            this.pnlMenu.Controls.Add(this.lblTrips);
            this.pnlMenu.Controls.Add(this.lblFishermen);
            this.pnlMenu.Controls.Add(this.pnlSeparator1);
            this.pnlMenu.Controls.Add(this.pbBait);
            this.pnlMenu.Controls.Add(this.pbCatches);
            this.pnlMenu.Controls.Add(this.pbTrips);
            this.pnlMenu.Controls.Add(this.pbFishermen);
            this.pnlMenu.Controls.Add(this.lblTitle);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlMenu.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(12)))), ((int)(((byte)(12)))));
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(180, 411);
            this.pnlMenu.TabIndex = 3;
            // 
            // pnlSeparator2
            // 
            this.pnlSeparator2.BackColor = System.Drawing.Color.Maroon;
            this.pnlSeparator2.ForeColor = System.Drawing.Color.Maroon;
            this.pnlSeparator2.Location = new System.Drawing.Point(40, 180);
            this.pnlSeparator2.Name = "pnlSeparator2";
            this.pnlSeparator2.Size = new System.Drawing.Size(100, 2);
            this.pnlSeparator2.TabIndex = 14;
            // 
            // lblLocations
            // 
            this.lblLocations.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLocations.AutoSize = true;
            this.lblLocations.BackColor = System.Drawing.Color.Transparent;
            this.lblLocations.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocations.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.lblLocations.Location = new System.Drawing.Point(35, 103);
            this.lblLocations.Name = "lblLocations";
            this.lblLocations.Size = new System.Drawing.Size(77, 13);
            this.lblLocations.TabIndex = 13;
            this.lblLocations.Text = "LOCATIONS";
            // 
            // pbLocations
            // 
            this.pbLocations.BackColor = System.Drawing.Color.Transparent;
            this.pbLocations.Image = global::AFL.Properties.Resources.locations;
            this.pbLocations.Location = new System.Drawing.Point(10, 100);
            this.pbLocations.Name = "pbLocations";
            this.pbLocations.Size = new System.Drawing.Size(20, 20);
            this.pbLocations.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbLocations.TabIndex = 12;
            this.pbLocations.TabStop = false;
            // 
            // lblBait
            // 
            this.lblBait.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBait.AutoSize = true;
            this.lblBait.BackColor = System.Drawing.Color.Transparent;
            this.lblBait.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBait.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.lblBait.Location = new System.Drawing.Point(35, 128);
            this.lblBait.Name = "lblBait";
            this.lblBait.Size = new System.Drawing.Size(35, 13);
            this.lblBait.TabIndex = 11;
            this.lblBait.Text = "BAIT";
            // 
            // lblCatches
            // 
            this.lblCatches.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCatches.AutoSize = true;
            this.lblCatches.BackColor = System.Drawing.Color.Transparent;
            this.lblCatches.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCatches.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.lblCatches.Location = new System.Drawing.Point(35, 153);
            this.lblCatches.Name = "lblCatches";
            this.lblCatches.Size = new System.Drawing.Size(64, 13);
            this.lblCatches.TabIndex = 10;
            this.lblCatches.Text = "CATCHES";
            // 
            // lblTrips
            // 
            this.lblTrips.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTrips.AutoSize = true;
            this.lblTrips.BackColor = System.Drawing.Color.Transparent;
            this.lblTrips.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrips.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.lblTrips.Location = new System.Drawing.Point(35, 78);
            this.lblTrips.Name = "lblTrips";
            this.lblTrips.Size = new System.Drawing.Size(44, 13);
            this.lblTrips.TabIndex = 9;
            this.lblTrips.Text = "TRIPS";
            // 
            // lblFishermen
            // 
            this.lblFishermen.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFishermen.AutoSize = true;
            this.lblFishermen.BackColor = System.Drawing.Color.Transparent;
            this.lblFishermen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFishermen.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.lblFishermen.Location = new System.Drawing.Point(35, 53);
            this.lblFishermen.Name = "lblFishermen";
            this.lblFishermen.Size = new System.Drawing.Size(79, 13);
            this.lblFishermen.TabIndex = 8;
            this.lblFishermen.Text = "FISHERMEN";
            this.lblFishermen.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lblFishermen_MouseClick);
            this.lblFishermen.MouseHover += new System.EventHandler(this.lblFishermen_MouseHover);
            // 
            // pnlSeparator1
            // 
            this.pnlSeparator1.BackColor = System.Drawing.Color.Maroon;
            this.pnlSeparator1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(151)))), ((int)(((byte)(32)))));
            this.pnlSeparator1.Location = new System.Drawing.Point(40, 40);
            this.pnlSeparator1.Name = "pnlSeparator1";
            this.pnlSeparator1.Size = new System.Drawing.Size(100, 2);
            this.pnlSeparator1.TabIndex = 7;
            // 
            // pbBait
            // 
            this.pbBait.BackColor = System.Drawing.Color.Transparent;
            this.pbBait.Image = global::AFL.Properties.Resources.baits;
            this.pbBait.Location = new System.Drawing.Point(10, 125);
            this.pbBait.Name = "pbBait";
            this.pbBait.Size = new System.Drawing.Size(20, 20);
            this.pbBait.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbBait.TabIndex = 6;
            this.pbBait.TabStop = false;
            // 
            // pbCatches
            // 
            this.pbCatches.BackColor = System.Drawing.Color.Transparent;
            this.pbCatches.Image = global::AFL.Properties.Resources.catches;
            this.pbCatches.Location = new System.Drawing.Point(10, 150);
            this.pbCatches.Name = "pbCatches";
            this.pbCatches.Size = new System.Drawing.Size(20, 20);
            this.pbCatches.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbCatches.TabIndex = 5;
            this.pbCatches.TabStop = false;
            // 
            // pbTrips
            // 
            this.pbTrips.BackColor = System.Drawing.Color.Transparent;
            this.pbTrips.Image = global::AFL.Properties.Resources.trips;
            this.pbTrips.Location = new System.Drawing.Point(10, 75);
            this.pbTrips.Name = "pbTrips";
            this.pbTrips.Size = new System.Drawing.Size(20, 20);
            this.pbTrips.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbTrips.TabIndex = 4;
            this.pbTrips.TabStop = false;
            // 
            // pbFishermen
            // 
            this.pbFishermen.BackColor = System.Drawing.Color.Transparent;
            this.pbFishermen.Image = global::AFL.Properties.Resources.fishermen;
            this.pbFishermen.Location = new System.Drawing.Point(10, 50);
            this.pbFishermen.Name = "pbFishermen";
            this.pbFishermen.Size = new System.Drawing.Size(20, 20);
            this.pbFishermen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbFishermen.TabIndex = 3;
            this.pbFishermen.TabStop = false;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(11, 15);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(158, 16);
            this.lblTitle.TabIndex = 3;
            this.lblTitle.Text = "A FISHERMAN\'S LOG";
            // 
            // flpFishermen
            // 
            this.flpFishermen.AutoScroll = true;
            this.flpFishermen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(18)))), ((int)(((byte)(18)))));
            this.flpFishermen.Location = new System.Drawing.Point(0, 50);
            this.flpFishermen.Name = "flpFishermen";
            this.flpFishermen.Size = new System.Drawing.Size(604, 314);
            this.flpFishermen.TabIndex = 4;
            // 
            // lblFishermenTitle
            // 
            this.lblFishermenTitle.AutoSize = true;
            this.lblFishermenTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblFishermenTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFishermenTitle.ForeColor = System.Drawing.Color.White;
            this.lblFishermenTitle.Location = new System.Drawing.Point(254, 15);
            this.lblFishermenTitle.Name = "lblFishermenTitle";
            this.lblFishermenTitle.Size = new System.Drawing.Size(96, 16);
            this.lblFishermenTitle.TabIndex = 14;
            this.lblFishermenTitle.Text = "FISHERMEN";
            // 
            // btnAddFisherman
            // 
            this.btnAddFisherman.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddFisherman.Location = new System.Drawing.Point(440, 370);
            this.btnAddFisherman.Name = "btnAddFisherman";
            this.btnAddFisherman.Size = new System.Drawing.Size(125, 35);
            this.btnAddFisherman.TabIndex = 13;
            this.btnAddFisherman.Text = "ADD FISHERMAN";
            this.btnAddFisherman.UseVisualStyleBackColor = true;
            this.btnAddFisherman.Click += new System.EventHandler(this.btnAddFisherman_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Maroon;
            this.panel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(151)))), ((int)(((byte)(32)))));
            this.panel1.Location = new System.Drawing.Point(102, 40);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(400, 2);
            this.panel1.TabIndex = 15;
            // 
            // pnlFishermen
            // 
            this.pnlFishermen.Controls.Add(this.panel1);
            this.pnlFishermen.Controls.Add(this.btnAddFisherman);
            this.pnlFishermen.Controls.Add(this.lblFishermenTitle);
            this.pnlFishermen.Controls.Add(this.flpFishermen);
            this.pnlFishermen.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlFishermen.Enabled = false;
            this.pnlFishermen.Location = new System.Drawing.Point(180, 0);
            this.pnlFishermen.Name = "pnlFishermen";
            this.pnlFishermen.Size = new System.Drawing.Size(604, 411);
            this.pnlFishermen.TabIndex = 0;
            this.pnlFishermen.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(18)))), ((int)(((byte)(18)))));
            this.ClientSize = new System.Drawing.Size(784, 411);
            this.Controls.Add(this.pnlFishermen);
            this.Controls.Add(this.pnlMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main";
            this.pnlMenu.ResumeLayout(false);
            this.pnlMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLocations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBait)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCatches)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTrips)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFishermen)).EndInit();
            this.pnlFishermen.ResumeLayout(false);
            this.pnlFishermen.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Panel pnlSeparator2;
        private System.Windows.Forms.Label lblLocations;
        private System.Windows.Forms.PictureBox pbLocations;
        private System.Windows.Forms.Label lblBait;
        private System.Windows.Forms.Label lblCatches;
        private System.Windows.Forms.Label lblTrips;
        private System.Windows.Forms.Label lblFishermen;
        private System.Windows.Forms.Panel pnlSeparator1;
        private System.Windows.Forms.PictureBox pbBait;
        private System.Windows.Forms.PictureBox pbCatches;
        private System.Windows.Forms.PictureBox pbTrips;
        private System.Windows.Forms.PictureBox pbFishermen;
        private System.Windows.Forms.Label lblTitle;
        private ListItemFisherman listItemFisherman1;
        private System.Windows.Forms.FlowLayoutPanel flpFishermen;
        private System.Windows.Forms.Label lblFishermenTitle;
        private System.Windows.Forms.Button btnAddFisherman;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlFishermen;
    }
}