﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFL
{
    interface LocationInformationInterface
    {
        void AddLocation(Image image, String name, String description);
        void AddLocation(String name, String description);
        void RemoveLocation(String name);
        void SaveLocations();
        void LoadLocations();
    }
}
