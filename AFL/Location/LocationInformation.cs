﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace AFL
{
    public class LocationInformation : LocationInformationInterface
    {
        public List<Location> listLocations;

        //Location information will be saved into this file.
        private const String DATA_FILENAME = "locationInfo.dat";

        public LocationInformation()
        {
            this.listLocations = new List<Location>();
        }

        //Creates and adds a Location object to listLocations.
        public void AddLocation(Image image, String name, String description)
        {
            foreach (Location l in listLocations)
            {
                if (l.Name == name)
                {
                    Console.WriteLine("You had already added " + name + " before.");
                    return;
                }
            }
            this.listLocations.Add(new Location(image, name,description));
            Console.WriteLine("Location succesfully added.");
        }

        //Creates and adds a Location object without an image to listLocations.
        public void AddLocation(String name, String description)
        {
            foreach (Location l in listLocations)
            {
                if (l.Name == name)
                {
                    Console.WriteLine("You had already added " + name + " before.");
                    return;
                }
            }
            this.listLocations.Add(new Location(Properties.Resources.locations, name, description));
            Console.WriteLine("Location succesfully added.");
        }

        //Removes a specific Location object from listLocation.
        public void RemoveLocation(String name)
        {
            foreach (Location l in listLocations)
            {
                if (l.Name == name)
                {
                    if (this.listLocations.Remove(l))
                    {
                        Console.WriteLine(name + " has been removed successfully.");
                        return;
                    }
                    else
                    {
                        Console.WriteLine("Unable to remove " + name);
                        return;
                    }
                }
            }
            Console.WriteLine(name + "had not been added before.");
        }

        //Saves location information into appropriate file.
        public void SaveLocations()
        {
            try
            {
                Stream stream = File.Open(DATA_FILENAME, FileMode.Create);
                BinaryFormatter bFormatter = new BinaryFormatter();

                Console.WriteLine("Writing location information.");
                bFormatter.Serialize(stream, this.listLocations);
                stream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("Unable to save location information.");
            }
        }

        //Loads location information from appropriate file.
        public void LoadLocations()
        {
            try
            {
                Stream stream = File.Open(DATA_FILENAME, FileMode.Open);
                BinaryFormatter bFormatter = new BinaryFormatter();

                Console.WriteLine("Reading location information.");
                this.listLocations = (List<Location>)bFormatter.Deserialize(stream);
                stream.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("The file that contains location information exists but there is a problem reading it.");
            }
        }
    }
}
