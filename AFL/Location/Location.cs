﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFL
{
    [Serializable]
    public class Location
    {
        public Location(Image Image, String Name, String Description)
        {
            this.Image = Image;
            this.Name = Name;
            this.Description = Description;
        }

        public Image Image { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }

        public override string ToString()
        {
            return ("LOCATION:\nName: " + Name);
        }
    }
}
