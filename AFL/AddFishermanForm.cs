﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AFL
{
    public partial class AddFishermanForm : Form
    {
        public AddFishermanForm()
        {
            InitializeComponent();
        }

        private string firstName;
        private string lastName;
        private DateTime birthDate;

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }

        //On click set values and close form.
        private void btnAdd_Click(object sender, EventArgs e)
        {
            FirstName = tbFirstName.Text;
            LastName = tbLastName.Text;
            BirthDate = dtpBirthDate.Value;
            this.Close();
        }

        //On click close form.
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
